package mdm.tw.com.mdm.Slapi;

/**
 * Created by DaNii on 04/11/2016.
 */

public class LogItem {
    private String localID;
    private String type;
    private String data;
    private String dateTime;

    public int getNoOfRetries() {
        return noOfRetries;
    }

    public void setNoOfRetries(int noOfRetries) {
        this.noOfRetries = noOfRetries;
    }

    private int noOfRetries;

    public LogItem() {
    }

    public LogItem(String type, String data, String dateTime, int noOfRetries) {
        this.type = type;
        this.data = data;
        this.dateTime = dateTime;
        this.noOfRetries = noOfRetries;
    }

    public String getLocalID() {
        return localID;
    }

    public void setLocalID(String localID) {
        this.localID = localID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
