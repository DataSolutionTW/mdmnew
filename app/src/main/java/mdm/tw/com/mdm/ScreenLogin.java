package mdm.tw.com.mdm;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import mdm.tw.com.mdm.db.DbHelper;
import mdm.tw.com.mdm.helpers.PrefrenceUtils;
import mdm.tw.com.mdm.helpers.UtilsFunctions;
import mdm.tw.com.mdm.interfaces.GetRecordValue;
import mdm.tw.com.mdm.services.MyService;
import mdm.tw.com.mdm.utils.DeviceAdmin;

/**
 * Created by developer on 5/15/18.
 */

public class ScreenLogin extends AppCompatActivity implements GetRecordValue{

    TelephonyManager telephonyManager = null;
    private static final int REQUEST_CODE = 0;
    private DevicePolicyManager mDPM;
    private ComponentName mAdminName;
    TextView imei, imsi, aid, txtSpeed, txtDirection, txtDistance, txtLatLng, txtAltitude;
//    private ArrayList<LocationItem> lists;
    DbHelper db;
    public static GetRecordValue objGetRecordValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        objGetRecordValue = this;

        addForceStopSolution();

        startService(new Intent(getApplicationContext(), MyService.class));
        getImei();

        imei = (TextView) findViewById(R.id.imei);
        imei.setText("Imei : "+PrefrenceUtils.getImei(getApplicationContext()));

        db = DbHelper.getInstance(getApplicationContext());
//        lists = new ArrayList<LocationItem>();
//        if (db.getTableRowsCount()>0) {
//            lists = db.getAllPointsFromLog_History2();
//        }

        imsi = (TextView) findViewById(R.id.imsi);
        imsi.setText("No of Records : " + db.getTableRowsCount());
        aid = (TextView) findViewById(R.id.and_id);
        txtSpeed = (TextView) findViewById(R.id.txtSpeed);
        txtDistance = (TextView) findViewById(R.id.txtDistance);
        txtDirection = (TextView) findViewById(R.id.txtDirection);
        txtAltitude = (TextView) findViewById(R.id.txtAltitude);
        txtLatLng = (TextView) findViewById(R.id.txtLatLng);
    }

    public void getImei() {


        if ((int) Build.VERSION.SDK_INT < 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                getDeviceId();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                handlePhoneStatePermission();
            } else {

                System.out.println("Permission is granted");
                getDeviceId();
            }

        }


//        imei.setText("IMEI: " + sid1);
//        imsi.setText("IMSI id: " + imei2 + "\nNumber of records:" + lists.size()+"\n"+a);
    }

    public void handlePhoneStatePermission() {

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_PHONE_STATE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        handleLocationPermission();
                        getDeviceId();
                        // permission is granted, open the camera
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                            handleLocationPermission();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void handleLocationPermission() {

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        handleLocationCoursePermission();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                            handleLocationCoursePermission();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void handleLocationCoursePermission() {

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void getDeviceId() {
        String deviceID = "";

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (telephonyManager != null) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    android.Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {

                deviceID = getDeviceID(telephonyManager);
                if(deviceID.contains("NONE")){

                    PrefrenceUtils.saveImei(this, UtilsFunctions.getSid());
                    System.out.println("Device id is : " + UtilsFunctions.getSid());

                }else{


                    System.out.println("Device id is : " + deviceID);
                    PrefrenceUtils.saveImei(this, deviceID);
                }

//                System.out.println("Device id using Settings"+  Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));

            }
        } else {


            PrefrenceUtils.saveImei(this, UtilsFunctions.getSid());
            System.out.println("Device id is : " + UtilsFunctions.getSid());
        }
    }

    String getDeviceID(TelephonyManager phonyManager) {

        String id = "";
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED) {

            id = phonyManager.getDeviceId();
        }
            if (id == null) {
                id = "not available";
            }

            int phoneType = phonyManager.getPhoneType();
            switch (phoneType) {
                case TelephonyManager.PHONE_TYPE_NONE:
                    return "" + id;

                case TelephonyManager.PHONE_TYPE_GSM:
                    return "" + id;

                case TelephonyManager.PHONE_TYPE_CDMA:
                    return "" + id;

                default:
                    return "" + id;
            }
        }

        public void addForceStopSolution(){
            try
            {
                // Initiate DevicePolicyManager.
                mDPM = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
                // Set DeviceAdminDemo Receiver for active the component with different option
                mAdminName = new ComponentName(this, DeviceAdmin.class);

                if (!mDPM.isAdminActive(mAdminName)) {
                    // try to become active
                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Click on Activate button to secure your application.");
                    startActivityForResult(intent, REQUEST_CODE);
                }
                else
                {
                    // Already is a device administrator, can do security operations now.
//                    mDPM.lockNow();
                }
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(REQUEST_CODE == requestCode)
        {
            if(requestCode == Activity.RESULT_OK)
            {
                System.out.println("Activity.RESULT_OK");
                // done with activate to Device Admin
            }
            else
            {
                System.out.println("Activity.RESULT_NOT_OK");
                // cancle it.
            }
        }
    }

    @Override
    public void showRecordValue(int value) {

        imsi.setText("No of Records : " + value);

    }

    @Override
    public void showSpeedValues(String speed) {
        txtSpeed.setText(speed);

    }

    @Override
    public void showDistanceValues(String distance) {
        txtDistance.setText(distance);

    }

    @Override
    public void showDirectionValues(String direction) {
        txtDirection.setText(direction);

    }

    @Override
    public void showAltitudeValues(String altitude) {
        txtAltitude.setText(altitude);

    }

    @Override
    public void showLatLngValues(String latLng) {
        txtLatLng.setText(latLng);

    }
}
