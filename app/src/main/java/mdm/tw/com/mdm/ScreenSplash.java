package mdm.tw.com.mdm;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import mdm.tw.com.mdm.utils.CommonObjects;

/**
 * Created by developer on 5/15/18.
 */

public class ScreenSplash extends AppCompatActivity {
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//
//        handleMapPermission();

//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        CommonObjects.setContext(getApplicationContext());
        //  CommonMethods.showProgressDialog(Splash.this);
        final Runnable runnable = new Runnable() {

            @Override
            public void run() {

                handleMapPermission();
//                Intent intent = new Intent(ScreenSplash.this, DashboardActivity.class);
//                startActivity(intent);
//                finish();

//                if (!CommonMethods.getStringPreference(
//                        ScreenSplash.this, Constants.KeyValues.AUTHORIZATION_DETAILS,
//                        Constants.KeyValues.AUTH_TYPE_AND_TOKEN, "").equals("")) {
//                    Intent intent = new Intent(ScreenSplash.this, LoginActivity.class);
//                    startActivity(intent);
//                    //           CommonMethods.hideProgressDialog();
//                    finish();
//                }
//                else{
//                    Intent intent = new Intent(ScreenSplash.this, LoginActivity.class);
//                    startActivity(intent);
//                    finish();
//
//                    //            CommonMethods.hideProgressDialog();
//                }

            }
        };
        handler = new Handler();
        handler.postDelayed(runnable, 2000);
    }
    public void handleMapPermission(){

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {

                        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                android.Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {
                            //Location Permission already granted
                            Intent intent = new Intent(ScreenSplash.this, DashboardActivity.class);
                            startActivity(intent);
                            finish();
                        }

                        // permission is granted, open the camera
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                            Intent intent = new Intent(ScreenSplash.this, DashboardActivity.class);
                            startActivity(intent);
                            finish();
//                            handleTimePermission();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }



}
