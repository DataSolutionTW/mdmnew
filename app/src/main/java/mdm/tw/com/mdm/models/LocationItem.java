package mdm.tw.com.mdm.models;

/**
 * Created by TW on 7/1/2017.
 */

public class LocationItem {


    String id;
    String lat;
    String Altitude;
    String Reason;
    String Speed;
    String Direction;
    String GPSFix;
    String NOS;

    String DeviceID;
    String dataUsage;

    public String getDataUsage() {
        return dataUsage;
    }

    public void setDataUsage(String dataUsage) {
        this.dataUsage = dataUsage;
    }

    public String getRcode() {
        return Rcode;
    }

    public void setRcode(String rcode) {
        Rcode = rcode;
    }

    String Rcode;
    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }


    public String getAltitude() {
        return Altitude;
    }

    public void setAltitude(String altitude) {
        Altitude = altitude;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getSpeed() {
        return Speed;
    }

    public void setSpeed(String speed) {
        Speed = speed;
    }

    public String getDirection() {
        return Direction;
    }

    public void setDirection(String direction) {
        Direction = direction;
    }

    public String getGPSFix() {
        return GPSFix;
    }

    public void setGPSFix(String GPSFix) {
        this.GPSFix = GPSFix;
    }

    public String getNOS() {
        return NOS;
    }

    public void setNOS(String NOS) {
        this.NOS = NOS;
    }

    public String getHDOP() {
        return HDOP;
    }

    public void setHDOP(String HDOP) {
        this.HDOP = HDOP;
    }

    String HDOP;
    public String getDatee() {
        return datee;
    }

    public void setDatee(String datee) {
        this.datee = datee;
    }

    String datee;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    String longi;
    int BatteryPercent;
    String GSMCellID;

    public int getBatteryPercent() {
        return BatteryPercent;
    }

    public void setBatteryPercent(int batteryPercent) {
        BatteryPercent = batteryPercent;
    }

    public String getGSMCellID() {
        return GSMCellID;
    }

    public void setGSMCellID(String GSMCellID) {
        this.GSMCellID = GSMCellID;
    }
}

