package mdm.tw.com.mdm.Slapi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


//import org.apache.commons.validator.routines.UrlValidator;


public class CommonMethods {
    private static ProgressDialog pgDialog;


   /* public static void showProgressDialog(Context nContext) {
        try {
            pgDialog = new ProgressDialog(nContext);
            pgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            pgDialog.setIndeterminate(true);
            pgDialog.setCancelable(false);
            pgDialog.show();
            pgDialog.setContentView(R.layout.dialog_loading);
        } catch (Exception e) {
        }
    }*/

    public static void hideProgressDialog() {
        try {
            if (pgDialog != null)
                pgDialog.dismiss();
        } catch (Exception e) {

        }
    }

    /*public static SimpleDateFormat getDateFormatLocationBased(Context context) {
        String userLocationCode = CommonMethods.getStringPreference(context, Constants.KeyValues.PROFILE, Constants.KeyValues.USER_LOCATION, "");
        SimpleDateFormat dateFormat;
        if (userLocationCode.equals("US")) {
            dateFormat = new SimpleDateFormat(Constants.US_TIME_FORMAT);
        } else {
            dateFormat = new SimpleDateFormat(Constants.NON_US_TIME_FORMAT);
        }
        return dateFormat;
    }*/

    /*public static String getUserLocationFromPref(Context context) {
        return CommonMethods.getStringPreference(context, Constants.KeyValues.PROFILE, Constants.KeyValues.USER_LOCATION, "");
    }

    public static String isUserFromUS(Context context) {
        String userLocation =  CommonMethods.getStringPreference(context, Constants.KeyValues.PROFILE, Constants.KeyValues.USER_LOCATION, "");
        if(userLocation.equals("US")){
            return "true";
        }
        return "false";
    }*/

//    public static Dialog showConfirmationDialog(Context nContext, String title, String message) {
//        View view = CommonMethods.createView(nContext,
//                R.layout.popup_confirmation, null);
//        TextView tvTittle = (TextView) view.findViewById(R.id.tvTitle);
//        TextView tvMessage = (TextView) view.findViewById(R.id.tvMessage);
//        if (!title.equals("")) {
//            tvTittle.setText(title);
//        }
//        if (!message.equals("")) {
//            tvMessage.setText(message);
//        }
//        Dialog nDialog = new Dialog(nContext, R.style.NewDialog);
//        nDialog.setContentView(view);
//        nDialog.setCancelable(true);
//        nDialog.show();
//        return nDialog;
//    }

    public static void callFragmentWithParameter(Fragment nFragment, int view, Bundle bundle, int enterAnim, int exitAnim, Context context) {
        FragmentManager fm;
        nFragment.setArguments(bundle);
        fm = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(enterAnim, exitAnim);
        fragmentTransaction.replace(view, nFragment);
        fragmentTransaction.commit();
    }

//    public static void callFragment(Fragment nFragment, int view,int enterAnim,int exitAnim,int popEnterAnim,int popExitAnim,Context context,boolean isBack) {
//        FragmentManager fm;
//        fm = ((FragmentActivity)context).getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
//        fragmentTransaction.setCustomAnimations(enterAnim, exitAnim,popEnterAnim,popExitAnim);
//        fragmentTransaction.replace(view, nFragment);
//        if(isBack)
//        {
//            fragmentTransaction.addToBackStack(null);
//        }
//        fragmentTransaction.commit();
//    }

    public static void callFragment(Fragment nFragment, int view, int enterAnim, int exitAnim, Context context) {
        FragmentManager fm;
        fm = ((FragmentActivity) context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        //fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        fragmentTransaction.setCustomAnimations(enterAnim, exitAnim);
        fragmentTransaction.replace(view, nFragment);
        fragmentTransaction.commit();
    }
/*
    public static String loadJSONFromAsset(String fileName, Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }*/

    /*public static void showToast(String message, int toastDuration) {
        try {
            View v = createView(CommonObjects.getContext(), R.layout.toast_layout, null);
            TextView nTextView = (TextView) v.findViewById(R.id.tvToast);
            nTextView.setText(message);
            Toast toast = Toast.makeText(CommonObjects.getContext(), message, toastDuration);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.setView(v);
            toast.show();
        } catch (Exception e) {
        }
    }*/

    public static void showNotification(Context nContext,
                                        Class<?> nActivityClass, String title, String message) {
        try {
//			Intent intent = new Intent(nContext, nActivityClass);
//			NotificationManager notificationManager = (NotificationManager) nContext
//					.getSystemService(Context.NOTIFICATION_SERVICE);
//			PendingIntent pendingIntent = PendingIntent.getActivity(nContext,
//					0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
//			NotificationCompat.Builder builder = new NotificationCompat.Builder(
//					nContext).setSmallIcon(R.drawable.ic_launcher)
//					.setContentTitle(title).setContentText(message)
//					.setContentIntent(pendingIntent).setAutoCancel(true);
//			notificationManager.notify(1, builder.build());
        } catch (Exception e) {
        }
    }

    public static void hideSoftKeyboard(View v, Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void callAnActivity(Context newContext,
                                      Class<?> newActivityClass) {
        try {
            Intent newIntent = new Intent(newContext, newActivityClass);
            newContext.startActivity(newIntent);
        } catch (Exception e) {
        }
    }

    public static void callAnActivityNew(Context newContext,
                                         Class<?> newActivityClass) {
        try {
            Intent newIntent = new Intent(newContext, newActivityClass);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            newContext.startActivity(newIntent);
        } catch (Exception e) {
        }
    }

    public static void callAnActivityForResult(Context newContext,
                                               Class<?> newActivityClass, int requestCode) {
        try {
            Intent newIntent = new Intent(newContext, newActivityClass);
            ((Activity) newContext).startActivityForResult(newIntent, requestCode);
        } catch (Exception e) {
        }
    }

    public static void callAnActivityForResultWithParameter(Context newContext,
                                                            Class<?> newActivityClass, int requestCode, String tag, String value) {
        try {
            Intent newIntent = new Intent(newContext, newActivityClass);
            newIntent.putExtra(tag, value);
            ((Activity) newContext).startActivityForResult(newIntent, requestCode);
        } catch (Exception e) {

        }
    }

    public static void callAnActivityWithParameter(Context newContext,
                                                   Class<?> newActivityClass, String tag, String value) {
        try {
            Intent newIntent = new Intent(newContext, newActivityClass);
            newIntent.putExtra(tag, value);
            newContext.startActivity(newIntent);
        } catch (Exception e) {

        }
    }

    public static void callAnActivityWithParameter(Context newContext,
                                                   Class<?> newActivityClass, String tag1, String value1, String tag2, String value2) {
        try {
            Intent newIntent = new Intent(newContext, newActivityClass);
            newIntent.putExtra(tag1, value1);
            newIntent.putExtra(tag2, value2);
            newContext.startActivity(newIntent);
        } catch (Exception e) {

        }
    }

    public static void callAnActivityWithParameter(Context newContext,
                                                   Class<?> newActivityClass, String tag1, String value1, String tag2, String value2, String tag3, String value3) {
        try {
            Intent newIntent = new Intent(newContext, newActivityClass);
            newIntent.putExtra(tag1, value1);
            newIntent.putExtra(tag2, value2);
            newIntent.putExtra(tag3, value3);
            newContext.startActivity(newIntent);
        } catch (Exception e) {

        }
    }

    public static View createView(Context context, int layout, ViewGroup parent) {
        try {
            LayoutInflater newLayoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            return newLayoutInflater.inflate(layout, parent, false);
        } catch (Exception e) {
            Log.i("IN CATCH ", e.toString());
            return null;
        }
    }

    public static String getStringPreference(Context nContext,
                                             String preferenceName, String preferenceItem, String defaultValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            return nPreferences.getString(preferenceItem, defaultValue);
        } catch (Exception e) {
            return "";
        }
    }

    public static int getIntPreference(Context nContext,
                                       String preferenceName, String preferenceItem, int deafaultValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            return nPreferences.getInt(preferenceItem, deafaultValue);
        } catch (Exception e) {
            return deafaultValue;
        }
    }

    public static long getLongPreference(Context nContext,
                                         String preferenceName, String preferenceItem, long deafaultValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            return nPreferences.getLong(preferenceItem, deafaultValue);
        } catch (Exception e) {
            return deafaultValue;
        }
    }

    public static Boolean getBooleanPreference(Context nContext,
                                               String preferenceName, String preferenceItem,
                                               Boolean defaultValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            return nPreferences.getBoolean(preferenceItem, defaultValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static void setStringPreference(Context nContext,
                                           String preferenceName, String preferenceItem,
                                           String preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putString(preferenceItem, preferenceItemValue);
            nEditor.commit();
        } catch (Exception e) {
        }
    }

    public static void setIntPreference(Context nContext,
                                        String preferenceName, String preferenceItem,
                                        int preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putInt(preferenceItem, preferenceItemValue);
            nEditor.commit();
        } catch (Exception e) {
        }
    }

    public static void setLongPreference(Context nContext,
                                         String preferenceName, String preferenceItem,
                                         long preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putLong(preferenceItem, preferenceItemValue);
            nEditor.commit();
        } catch (Exception e) {
        }
    }

    public static void setBooleanPreference(Context nContext,
                                            String preferenceName, String preferenceItem,
                                            Boolean preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putBoolean(preferenceItem, preferenceItemValue);
            nEditor.commit();
        } catch (Exception e) {
        }
    }

    public static String removeSpacing(String phoneNumber) {
        try {
            phoneNumber = phoneNumber.replace("-", "");
            phoneNumber = phoneNumber.replace(" ", "");
            removeNonDigits(phoneNumber);
            if (phoneNumber.length() >= 11) {
                phoneNumber = phoneNumber.substring(phoneNumber.length() - 11);
            }
            return phoneNumber;
        } catch (Exception e) {
            return phoneNumber;
        }
    }

    public static String removeNonDigits(String text) {
        try {
            int length = text.length();
            StringBuffer buffer = new StringBuffer(length);
            for (int i = 0; i < length; i++) {
                char ch = text.charAt(i);
                if (Character.isDigit(ch)) {
                    buffer.append(ch);
                }
            }
            return buffer.toString();
        } catch (Exception e) {
            return text;
        }
    }

    public static int getBooleanInt(boolean bol) {
        try {
            if (bol)
                return 1;
            else
                return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean getBoolean(int bolInt) {
        try {
            if (bolInt == 1)
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isNetworkAvailable(Context nContext) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) nContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            return activeNetworkInfo != null;
        } catch (Exception e) {
        }
        return false;
    }

    public static boolean isWifiConnected(Context nContext) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) nContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = null;
            if (connectivityManager != null) {
                networkInfo = connectivityManager
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            }
            return networkInfo == null ? false : networkInfo.isConnected();
        } catch (Exception e) {
        }
        return false;
    }

    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static int getRandom(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min) + min;
    }

    public static int getFontSize(Activity activity, float var) {

        DisplayMetrics dMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);

        // lets try to get them back a font size realtive to the pixel width of the screen
        final float WIDE = activity.getResources().getDisplayMetrics().heightPixels;
        int valueWide = (int) (WIDE / 32.0f / (dMetrics.scaledDensity));
        return (int) ((float) (valueWide * var));
    }

    public static int getDeviceWidth(Context nConext) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) nConext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    public static int getDeviceHeight(Context nConext) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) nConext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }

    public static boolean isEmailValid(String email) {
        try {
            Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }

    public static void threadSleep(int value) {
        try {
            Thread.sleep(value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

   /* public static void fadeOut(final View view) {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            AlphaAnimation alpha = new AlphaAnimation(1f, 0.5f);
            alpha.setDuration(0);
            alpha.setFillAfter(true);
            view.startAnimation(alpha);
        } else {
            view.animate()
                    .alpha(0.5f)
                    .setDuration(0)
                    .setListener(null);
        }
    }*/

   /* public static void fadeIn(View view) {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setVisibility(View.VISIBLE);
            AlphaAnimation alpha = new AlphaAnimation(0.5f, 1f);
            alpha.setDuration(0);
            alpha.setFillAfter(true);
            view.startAnimation(alpha);
        } else {
            view.setAlpha(0.5f);
            view.animate()
                    .alpha(1f)
                    .setDuration(0)
                    .setListener(null);
        }
    }*/

    public static boolean checkForNetworkProvider(
            LocationManager nLocationManager, Context nContext) {
        if (nLocationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                || (CommonMethods.isNetworkAvailable(nContext) || CommonMethods
                .isWifiConnected(nContext))) {
            return true;
        }
        return false;
    }


    public static String getPhone(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String code = tm.getSimCountryIso();
        if (code.equals("")) {
            code = tm.getNetworkCountryIso();
        }
        if (country2phone.get(code.toUpperCase()) != null) {
            return country2phone.get(code.toUpperCase());
        }
        return "+";
    }

    public static Map<String, String> getAll() {
        return country2phone;
    }

    private static Map<String, String> country2phone = new HashMap<String, String>();

    static {
        country2phone.put("AF", "+93");
        country2phone.put("AL", "+355");
        country2phone.put("DZ", "+213");
        country2phone.put("AD", "+376");
        country2phone.put("AO", "+244");
        country2phone.put("AG", "+1-268");
        country2phone.put("AR", "+54");
        country2phone.put("AM", "+374");
        country2phone.put("AU", "+61");
        country2phone.put("AT", "+43");
        country2phone.put("AZ", "+994");
        country2phone.put("BS", "+1-242");
        country2phone.put("BH", "+973");
        country2phone.put("BD", "+880");
        country2phone.put("BB", "+1-246");
        country2phone.put("BY", "+375");
        country2phone.put("BE", "+32");
        country2phone.put("BZ", "+501");
        country2phone.put("BJ", "+229");
        country2phone.put("BT", "+975");
        country2phone.put("BO", "+591");
        country2phone.put("BA", "+387");
        country2phone.put("BW", "+267");
        country2phone.put("BR", "+55");
        country2phone.put("BN", "+673");
        country2phone.put("BG", "+359");
        country2phone.put("BF", "+226");
        country2phone.put("BI", "+257");
        country2phone.put("KH", "+855");
        country2phone.put("CM", "+237");
        country2phone.put("CA", "+1");
        country2phone.put("CV", "+238");
        country2phone.put("CF", "+236");
        country2phone.put("TD", "+235");
        country2phone.put("CL", "+56");
        country2phone.put("CN", "+86");
        country2phone.put("CO", "+57");
        country2phone.put("KM", "+269");
        country2phone.put("CD", "+243");
        country2phone.put("CG", "+242");
        country2phone.put("CR", "+506");
        country2phone.put("CI", "+225");
        country2phone.put("HR", "+385");
        country2phone.put("CU", "+53");
        country2phone.put("CY", "+357");
        country2phone.put("CZ", "+420");
        country2phone.put("DK", "+45");
        country2phone.put("DJ", "+253");
        country2phone.put("DM", "+1-767");
        country2phone.put("DO", "+1-809and1-829");
        country2phone.put("EC", "+593");
        country2phone.put("EG", "+20");
        country2phone.put("SV", "+503");
        country2phone.put("GQ", "+240");
        country2phone.put("ER", "+291");
        country2phone.put("EE", "+372");
        country2phone.put("ET", "+251");
        country2phone.put("FJ", "+679");
        country2phone.put("FI", "+358");
        country2phone.put("FR", "+33");
        country2phone.put("GA", "+241");
        country2phone.put("GM", "+220");
        country2phone.put("GE", "+995");
        country2phone.put("DE", "+49");
        country2phone.put("GH", "+233");
        country2phone.put("GR", "+30");
        country2phone.put("GD", "+1-473");
        country2phone.put("GT", "+502");
        country2phone.put("GN", "+224");
        country2phone.put("GW", "+245");
        country2phone.put("GY", "+592");
        country2phone.put("HT", "+509");
        country2phone.put("HN", "+504");
        country2phone.put("HU", "+36");
        country2phone.put("IS", "+354");
        country2phone.put("IN", "+91");
        country2phone.put("ID", "+62");
        country2phone.put("IR", "+98");
        country2phone.put("IQ", "+964");
        country2phone.put("IE", "+353");
        country2phone.put("IL", "+972");
        country2phone.put("IT", "+39");
        country2phone.put("JM", "+1-876");
        country2phone.put("JP", "+81");
        country2phone.put("JO", "+962");
        country2phone.put("KZ", "+7");
        country2phone.put("KE", "+254");
        country2phone.put("KI", "+686");
        country2phone.put("KP", "+850");
        country2phone.put("KR", "+82");
        country2phone.put("KW", "+965");
        country2phone.put("KG", "+996");
        country2phone.put("LA", "+856");
        country2phone.put("LV", "+371");
        country2phone.put("LB", "+961");
        country2phone.put("LS", "+266");
        country2phone.put("LR", "+231");
        country2phone.put("LY", "+218");
        country2phone.put("LI", "+423");
        country2phone.put("LT", "+370");
        country2phone.put("LU", "+352");
        country2phone.put("MK", "+389");
        country2phone.put("MG", "+261");
        country2phone.put("MW", "+265");
        country2phone.put("MY", "+60");
        country2phone.put("MV", "+960");
        country2phone.put("ML", "+223");
        country2phone.put("MT", "+356");
        country2phone.put("MH", "+692");
        country2phone.put("MR", "+222");
        country2phone.put("MU", "+230");
        country2phone.put("MX", "+52");
        country2phone.put("FM", "+691");
        country2phone.put("MD", "+373");
        country2phone.put("MC", "+377");
        country2phone.put("MN", "+976");
        country2phone.put("ME", "+382");
        country2phone.put("MA", "+212");
        country2phone.put("MZ", "+258");
        country2phone.put("MM", "+95");
        country2phone.put("NA", "+264");
        country2phone.put("NR", "+674");
        country2phone.put("NP", "+977");
        country2phone.put("NL", "+31");
        country2phone.put("NZ", "+64");
        country2phone.put("NI", "+505");
        country2phone.put("NE", "+227");
        country2phone.put("NG", "+234");
        country2phone.put("NO", "+47");
        country2phone.put("OM", "+968");
        country2phone.put("PK", "+92");
        country2phone.put("PW", "+680");
        country2phone.put("PA", "+507");
        country2phone.put("PG", "+675");
        country2phone.put("PY", "+595");
        country2phone.put("PE", "+51");
        country2phone.put("PH", "+63");
        country2phone.put("PL", "+48");
        country2phone.put("PT", "+351");
        country2phone.put("QA", "+974");
        country2phone.put("RO", "+40");
        country2phone.put("RU", "+7");
        country2phone.put("RW", "+250");
        country2phone.put("KN", "+1-869");
        country2phone.put("LC", "+1-758");
        country2phone.put("VC", "+1-784");
        country2phone.put("WS", "+685");
        country2phone.put("SM", "+378");
        country2phone.put("ST", "+239");
        country2phone.put("SA", "+966");
        country2phone.put("SN", "+221");
        country2phone.put("RS", "+381");
        country2phone.put("SC", "+248");
        country2phone.put("SL", "+232");
        country2phone.put("SG", "+65");
        country2phone.put("SK", "+421");
        country2phone.put("SI", "+386");
        country2phone.put("SB", "+677");
        country2phone.put("SO", "+252");
        country2phone.put("ZA", "+27");
        country2phone.put("ES", "+34");
        country2phone.put("LK", "+94");
        country2phone.put("SD", "+249");
        country2phone.put("SR", "+597");
        country2phone.put("SZ", "+268");
        country2phone.put("SE", "+46");
        country2phone.put("CH", "+41");
        country2phone.put("SY", "+963");
        country2phone.put("TJ", "+992");
        country2phone.put("TZ", "+255");
        country2phone.put("TH", "+66");
        country2phone.put("TL", "+670");
        country2phone.put("TG", "+228");
        country2phone.put("TO", "+676");
        country2phone.put("TT", "+1-868");
        country2phone.put("TN", "+216");
        country2phone.put("TR", "+90");
        country2phone.put("TM", "+993");
        country2phone.put("TV", "+688");
        country2phone.put("UG", "+256");
        country2phone.put("UA", "+380");
        country2phone.put("AE", "+971");
        country2phone.put("GB", "+44");
        country2phone.put("US", "+1");
        country2phone.put("UY", "+598");
        country2phone.put("UZ", "+998");
        country2phone.put("VU", "+678");
        country2phone.put("VA", "+379");
        country2phone.put("VE", "+58");
        country2phone.put("VN", "+84");
        country2phone.put("YE", "+967");
        country2phone.put("ZM", "+260");
        country2phone.put("ZW", "+263");
        country2phone.put("GE", "+995");
        country2phone.put("TW", "+886");
        country2phone.put("AZ", "+374-97");
        country2phone.put("CY", "+90-392");
        country2phone.put("MD", "+373-533");
        country2phone.put("SO", "+252");
        country2phone.put("GE", "+995");
        country2phone.put("CX", "+61");
        country2phone.put("CC", "+61");
        country2phone.put("NF", "+672");
        country2phone.put("NC", "+687");
        country2phone.put("PF", "+689");
        country2phone.put("YT", "+262");
        country2phone.put("GP", "+590");
        country2phone.put("GP", "+590");
        country2phone.put("PM", "+508");
        country2phone.put("WF", "+681");
        country2phone.put("CK", "+682");
        country2phone.put("NU", "+683");
        country2phone.put("TK", "+690");
        country2phone.put("GG", "+44");
        country2phone.put("IM", "+44");
        country2phone.put("JE", "+44");
        country2phone.put("AI", "+1-264");
        country2phone.put("BM", "+1-441");
        country2phone.put("IO", "+246");
        country2phone.put("", "+357");
        country2phone.put("VG", "+1-284");
        country2phone.put("KY", "+1-345");
        country2phone.put("FK", "+500");
        country2phone.put("GI", "+350");
        country2phone.put("MS", "+1-664");
        country2phone.put("SH", "+290");
        country2phone.put("TC", "+1-649");
        country2phone.put("MP", "+1-670");
        country2phone.put("PR", "+1-787and1-939");
        country2phone.put("AS", "+1-684");
        country2phone.put("GU", "+1-671");
        country2phone.put("VI", "+1-340");
        country2phone.put("HK", "+852");
        country2phone.put("MO", "+853");
        country2phone.put("FO", "+298");
        country2phone.put("GL", "+299");
        country2phone.put("GF", "+594");
        country2phone.put("GP", "+590");
        country2phone.put("MQ", "+596");
        country2phone.put("RE", "+262");
        country2phone.put("AX", "+358-18");
        country2phone.put("AW", "+297");
        country2phone.put("AN", "+599");
        country2phone.put("SJ", "+47");
        country2phone.put("AC", "+247");
        country2phone.put("TA", "+290");
        country2phone.put("CS", "+381");
        country2phone.put("PS", "+970");
        country2phone.put("EH", "+212");
    }

    public static Bitmap createImageFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public static Bitmap createImageFromMask(Context context, int maskImage, int originalImage) {
        Bitmap mask = BitmapFactory.decodeResource(context.getResources(), maskImage);
        Bitmap original = BitmapFactory.decodeResource(context.getResources(), originalImage);
        original = Bitmap.createScaledBitmap(original, mask.getWidth(), mask.getHeight(), true);
        Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mCanvas.drawBitmap(original, 0, 0, null);
        mCanvas.drawBitmap(mask, 0, 0, paint);
        paint.setXfermode(null);
        return result;
    }

    public static void setImage(ImageView nImageView, byte[] nImageBytes, Point size, boolean isRoundedCorners, int pixels) {
        new LoadImageTask(nImageView, nImageBytes, size, isRoundedCorners, pixels).execute();
    }

    private static class LoadImageTask extends AsyncTask<String, Void, Boolean> {
        private ImageView nImageView;
        private byte[] mCameraData;
        private Bitmap nBitmap;
        private Point nSize;
        private boolean isRoundedCorners;
        private boolean isRounded;
        private int pixels;
        private String nImageString;
        private int rotate;

        public LoadImageTask(ImageView nImageView, String nImageString, Point nSize, boolean isRounded, int rotate) {
            this.nImageView = nImageView;
            this.nImageString = nImageString;
            this.nSize = nSize;
            this.isRounded = isRounded;
            this.rotate = rotate;
        }

        public LoadImageTask(ImageView nImageView, byte[] mCameraData, Point nSize, boolean isRoundedCorners, int pixels) {
            this.nImageView = nImageView;
            this.mCameraData = mCameraData;
            this.nSize = nSize;
            this.isRoundedCorners = isRoundedCorners;
            this.pixels = pixels;
        }

        public LoadImageTask(ImageView nImageView, byte[] mCameraData, Point nSize, boolean isRounded) {
            this.nImageView = nImageView;
            this.mCameraData = mCameraData;
            this.nSize = nSize;
            this.isRounded = isRounded;
        }

        public LoadImageTask(ImageView nImageView, byte[] mCameraData, Point nSize) {
            this.nImageView = nImageView;
            this.mCameraData = mCameraData;
            this.nSize = nSize;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            if (mCameraData != null) {
                nBitmap = loadBitmap(mCameraData);
            } else {
                nBitmap = loadBitmap(nImageString);
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (nBitmap != null) {
                nImageView.setImageBitmap(nBitmap);
            }
        }

        private Bitmap loadBitmap(byte[] cameraData) {
            Bitmap bitmap = Bitmap.createScaledBitmap(
                    BitmapFactory.decodeByteArray(mCameraData, 0, mCameraData.length), nSize.y, nSize.x, true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            if (isRoundedCorners) {
                bitmap = getRoundedCornerBitmap(bitmap, pixels);
            } else if (isRounded) {
                bitmap = getRoundedShapeBitmap(bitmap);
            }
            Matrix nMatrix = new Matrix();
            nMatrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), nMatrix, true);
            return bitmap;
        }

        private Bitmap loadBitmap(String imageString) {
            Bitmap bitmap = null;
            if (!imageString.contains("http")) {
//                bitmap = Bitmap.createScaledBitmap(
//                        BitmapFactory.decodeFile(imageString), nSize.x, nSize.y, true);
                bitmap = lessResolution(imageString, nSize.x, nSize.y);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                bitmap = rotateBitmap(bitmap, getCameraPhotoOrientation(imageString));
                bitmap = rotateBitmap(bitmap, rotate);
            } else {
                try {
                    InputStream in = new URL(imageString).openStream();
                    bitmap = BitmapFactory.decodeStream(in);
                    bitmap = Bitmap.createScaledBitmap(bitmap, nSize.x, nSize.y, true);
//                    bitmap = rotateBitmap(bitmap, 360f);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (bitmap != null) {
                if (isRoundedCorners) {
                    bitmap = getRoundedCornerBitmap(bitmap, pixels);
                } else if (isRounded) {
                    bitmap = getRoundedShapeBitmap(bitmap);
                }
                Matrix nMatrix = new Matrix();
//                nMatrix.postRotate(getCameraPhotoOrientation(imageString));
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), nMatrix, true);
            }
            return bitmap;
        }

        private Bitmap lessResolution(String filePath, int width, int height) {
            int reqHeight = height;
            int reqWidth = width;
            BitmapFactory.Options options = new BitmapFactory.Options();

            // First decode with inJustDecodeBounds=true to check dimensions
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;

            return BitmapFactory.decodeFile(filePath, options);
        }

        private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {
                // Calculate ratios of height and width to requested height and width
                final int heightRatio = Math.round((float) height / (float) reqHeight);
                final int widthRatio = Math.round((float) width / (float) reqWidth);

                // Choose the smallest ratio as inSampleSize value, this will guarantee
                // a final image with both dimensions larger than or equal to the
                // requested height and width.
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
            return inSampleSize;
        }

        public int getCameraPhotoOrientation(String imagePath) {
            int rotate = 0;
            try {
//                context.getContentResolver().notifyChange(imageUri, null);
//                File imageFile = new File(imagePath);

                ExifInterface exif = new ExifInterface(imagePath);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotate = 270;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotate = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotate = 90;
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return rotate;
        }

        public Bitmap rotateBitmap(Bitmap source, float angle) {
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        }

        private Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                    .getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = pixels;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        }

        private Bitmap getRoundedShapeBitmap(Bitmap bitmap) {
            int targetWidth = nSize.x;
            int targetHeight = nSize.y;
            Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                    targetHeight, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(targetBitmap);
            Path path = new Path();
            path.addCircle(((float) targetWidth - 1) / 2,
                    ((float) targetHeight - 1) / 2,
                    (Math.min(((float) targetWidth),
                            ((float) targetHeight)) / 2),
                    Path.Direction.CCW);
            canvas.clipPath(path);
            Bitmap sourceBitmap = bitmap;
            canvas.drawBitmap(sourceBitmap,
                    new Rect(0, 0, sourceBitmap.getWidth(),
                            sourceBitmap.getHeight()),
                    new Rect(0, 0, targetWidth, targetHeight), null);
            return targetBitmap;
        }
    }
   /* private static class LoadImageTask extends AsyncTask<String, Void, Boolean> {
        private ImageView nImageView;
        private byte[] mCameraData;
        private Bitmap nBitmap;
        private Point nSize;
        private boolean isRoundedCorners;
        private boolean isRounded;
        private int pixels;
        private String nImageString;

        public LoadImageTask(ImageView nImageView, String nImageString, Point nSize, boolean isRounded) {
            this.nImageView = nImageView;
            this.nImageString = nImageString;
            this.nSize = nSize;
            this.isRounded = isRounded;
        }

        public LoadImageTask(ImageView nImageView, byte[] mCameraData, Point nSize, boolean isRoundedCorners, int pixels) {
            this.nImageView = nImageView;
            this.mCameraData = mCameraData;
            this.nSize = nSize;
            this.isRoundedCorners = isRoundedCorners;
            this.pixels = pixels;
        }

        public LoadImageTask(ImageView nImageView, byte[] mCameraData, Point nSize, boolean isRounded) {
            this.nImageView = nImageView;
            this.mCameraData = mCameraData;
            this.nSize = nSize;
            this.isRounded = isRounded;
        }

        public LoadImageTask(ImageView nImageView, byte[] mCameraData, Point nSize) {
            this.nImageView = nImageView;
            this.mCameraData = mCameraData;
            this.nSize = nSize;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            if (mCameraData != null) {
                nBitmap = loadBitmap(mCameraData);
            } else {
                nBitmap = loadBitmap(nImageString);
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (nBitmap != null) {
                nImageView.setImageBitmap(nBitmap);
            }
        }

        private Bitmap loadBitmap(byte[] cameraData) {
            Bitmap bitmap = Bitmap.createScaledBitmap(
                    BitmapFactory.decodeByteArray(mCameraData, 0, mCameraData.length), nSize.y, nSize.x, true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            if (isRoundedCorners) {
                bitmap = getRoundedCornerBitmap(bitmap, pixels);
            } else if (isRounded) {
                bitmap = getRoundedShapeBitmap(bitmap);
            }
            Matrix nMatrix = new Matrix();
            nMatrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), nMatrix, true);
            return bitmap;
        }

        private Bitmap loadBitmap(String imageString) {
            Bitmap bitmap = null;
            if (!imageString.contains("http")) {
                bitmap = Bitmap.createScaledBitmap(
                        BitmapFactory.decodeFile(imageString), nSize.y, nSize.x, true);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            } else {
                try {
                    InputStream in = new java.net.URL(imageString).openStream();
                    bitmap = BitmapFactory.decodeStream(in);
                    bitmap = Bitmap.createScaledBitmap(bitmap, nSize.x, nSize.y, true);
                    bitmap = rotateBitmap(bitmap, 270f);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (bitmap != null) {
                if (isRoundedCorners) {
                    bitmap = getRoundedCornerBitmap(bitmap, pixels);
                } else if (isRounded) {
                    bitmap = getRoundedShapeBitmap(bitmap);
                }
                Matrix nMatrix = new Matrix();
                nMatrix.postRotate(90);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), nMatrix, true);
            }
            return bitmap;
        }

        public Bitmap rotateBitmap(Bitmap source, float angle) {
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        }

        private Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                    .getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = pixels;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        }

        private Bitmap getRoundedShapeBitmap(Bitmap bitmap) {
            int targetWidth = nSize.x;
            int targetHeight = nSize.y;
            Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                    targetHeight, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(targetBitmap);
            Path path = new Path();
            path.addCircle(((float) targetWidth - 1) / 2,
                    ((float) targetHeight - 1) / 2,
                    (Math.min(((float) targetWidth),
                            ((float) targetHeight)) / 2),
                    Path.Direction.CCW);
            canvas.clipPath(path);
            Bitmap sourceBitmap = bitmap;
            canvas.drawBitmap(sourceBitmap,
                    new Rect(0, 0, sourceBitmap.getWidth(),
                            sourceBitmap.getHeight()),
                    new Rect(0, 0, targetWidth, targetHeight), null);
            return targetBitmap;
        }
    }*/

    /*public static void setImage(ImageView nImageView, String nImageString, Point size, boolean isRounded) {
        new LoadImageTask(nImageView, nImageString, size, isRounded).execute();
    }*/

    public static void setImage(ImageView nImageView, String nImageString, Point size, boolean isRounded, int rotate) {
        new LoadImageTask(nImageView, nImageString, size, isRounded, rotate).execute();
    }

    public static void setImage(ImageView nImageView, byte[] mCameraData, Point nSize, boolean isRounded) {
        new LoadImageTask(nImageView, mCameraData, nSize, isRounded).execute();
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
/*
    public static boolean isValidUrl(String url) {
        UrlValidator defaultValidator = new UrlValidator(); // default schemes
        if (defaultValidator.isValid(url)) {
            return true;
        } else {
            return false;
        }
    }*/

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTodaysDate() {
        Calendar date = Calendar.getInstance();
        String hour = date.get(Calendar.HOUR_OF_DAY) + "";
        String minute = date.get(Calendar.MINUTE) + "";
        String seconds = date.get(Calendar.SECOND) + "";
        //String milliSeconds = date.get(Calendar.MILLISECOND) + "";
        /*if (hour.length() == 0) {
            hour = "0" + hour;
        }
        if (minute.length() == 0) {
            minute = "0" + minute;
        }*/
        //String dateToday = getDay(date.get(Calendar.DAY_OF_WEEK)) + ", " + date.get(Calendar.DAY_OF_MONTH) + " " + getMonth(date.get(Calendar.MONTH)) + " " + hour + ":" + minute + ", " + date.get(Calendar.YEAR);
        // String dateToday = /*getDay(date.get(Calendar.DAY_OF_WEEK)) + ", " + */date.get(Calendar.DAY_OF_MONTH) + "-" + getMonth(date.get(Calendar.MONTH)) + "-" + date.get(Calendar.YEAR) + " " + hour + ":" + minute;
        String dateToday = date.get(Calendar.YEAR) + "-" + getMonthInNum(date.get(Calendar.MONTH))
                + "-" + date.get(Calendar.DAY_OF_MONTH) + " " + hour + ":" + minute + ":" + seconds;
        return dateToday;
    }

    public static String getTodaysDateForServer() {
        Calendar date = Calendar.getInstance();
        String hour = date.get(Calendar.HOUR_OF_DAY) + "";
        String minute = date.get(Calendar.MINUTE) + "";
        String seconds = date.get(Calendar.SECOND) + "";
        //String am_pm = date.get(Calendar.AM_PM) + "";
        String dayOfMonth = date.get(Calendar.DAY_OF_MONTH) + "";

        /*if (am_pm == "0") {
            am_pm = "AM";
        } else {
            am_pm = "PM";
        }*/

        if (hour.length() == 0) {
            hour = "0" + hour;
        }
        if (minute.length() == 1) {
            minute = "0" + minute;
        }
        if (dayOfMonth.length() == 1) {
            dayOfMonth = "0" + dayOfMonth;
        }


        //String dateToday = getDay(date.get(Calendar.DAY_OF_WEEK)) + ", " + date.get(Calendar.DAY_OF_MONTH) + " " + getMonth(date.get(Calendar.MONTH)) + " " + hour + ":" + minute + ", " + date.get(Calendar.YEAR);
        String dateToday = getMonthInNum(date.get(Calendar.MONTH)) + "/" + dayOfMonth + "/" + date.get(Calendar.YEAR) + " " + hour + ":" + minute + ":" + seconds;// + " " + am_pm;
        return dateToday;
    }

    public static String getDay(int id) {
        switch (id) {
            case 1:
                return "Sunday";

            case 2:
                return "Monday";

            case 3:
                return "Tuesday";

            case 4:
                return "Wednesday";

            case 5:
                return "Thursday";

            case 6:
                return "Friday";

            case 7:
                return "Saturday";

        }

        return "";
    }

    public static String getMonth(int id) {
        switch (id + 1) {
            case 1:
                return "January";

            case 2:
                return "February";

            case 3:
                return "March";

            case 4:
                return "Aprail";

            case 5:
                return "May";

            case 6:
                return "June";

            case 7:
                return "July";

            case 8:
                return "August";

            case 9:
                return "September";

            case 10:
                return "Octtobar";

            case 11:
                return "November";

            case 12:
                return "December";
        }

        return "";
    }

    public static String getMonthInNum(int id) {
        switch (id + 1) {
            case 1:
                return "1";

            case 2:
                return "2";

            case 3:
                return "3";

            case 4:
                return "4";

            case 5:
                return "5";

            case 6:
                return "6";

            case 7:
                return "7";

            case 8:
                return "8";

            case 9:
                return "9";

            case 10:
                return "10";

            case 11:
                return "11";

            case 12:
                return "12";
        }

        return "";
    }

    public static void openImageFromURI(Context context, String filePath) {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri imgUri = Uri.parse("file://" + filePath);
        intent.setDataAndType(imgUri, "image/*");
        ((Activity) context).startActivity(intent);

    }

    public static void openImageFromURL(Context context, String URL) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        //Uri imgUri = Uri.parse("file://" + filePath);
        Uri imgUri = Uri.parse(URL);
        intent.setDataAndType(imgUri, "image/*");
        ((Activity) context).startActivity(intent);

    }

    public static String getTodaysDateForLogFile() {
        Calendar date = Calendar.getInstance();
        String hour = date.get(Calendar.HOUR_OF_DAY) + "";
        String minute = date.get(Calendar.MINUTE) + "";
        String second = date.get(Calendar.SECOND) + "";
        String milliSeconds = date.get(Calendar.MILLISECOND) + "";
        if (hour.length() == 0) {
            hour = "0" + hour;
        }
        if (minute.length() == 0) {
            minute = "0" + minute;
        }
        //String dateToday = getDay(date.get(Calendar.DAY_OF_WEEK)) + ", " + date.get(Calendar.DAY_OF_MONTH) + " " + getMonth(date.get(Calendar.MONTH)) + " " + hour + ":" + minute + ", " + date.get(Calendar.YEAR);
        String dateToday = /*getDay(date.get(Calendar.DAY_OF_WEEK)) + ", " + */date.get(Calendar.DAY_OF_MONTH) + "-" + getMonth(date.get(Calendar.MONTH))
                + "-" + date.get(Calendar.YEAR) + " " + hour + ":" + minute + ":" + second + ":" + milliSeconds;
        return dateToday;
    }

    public static void appendLog(String text) {
        File logFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() , "iNav_Helper.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
