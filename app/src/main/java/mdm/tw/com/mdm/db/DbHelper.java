package mdm.tw.com.mdm.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import mdm.tw.com.mdm.models.Internet;
import mdm.tw.com.mdm.models.LocationItem;

/**
 * Created by waqas on 02/06/2018.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "MDM";
    private static final int DB_VERSION = 1;
//    private static final int DB_VERSION = 2;
    private static Context context;
//    SQLiteDatabase dataBase = null;
    private static DbHelper sInstance;

    public static synchronized DbHelper getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new DbHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    /**
     * Constructor should be private to prevent direct instantiation.
     * make call to static method "getInstance()" instead.
     */
    private DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }


    private static class Userlocation {
        private static final String TABLE_NAME = "LocationHistory";
        private static final String COL_ID = "id";
        private static final String COL_POINT_CURRENT_LAT = "CurrentLat";
        private static final String COL_POINT_CURRENT_LNG = "CurrentLng";

        private static final String COL_POINT_DATE_TIME = "datee";
        private static final String COL_DeviceID = "DeviceID";
        private static final String COL_Altitude = "Altitude";
        private static final String COL_Reason = "Reason";
        private static final String COL_Speed = "Speed";
        private static final String COL_Direction = "Direction";
        private static final String COL_GPSFix = "GPSFix";
        private static final String COL_NOS = "NOS";
        private static final String COL_HDOP = "HDOP";
        private static final String COL_BS = "BatteryStatus";
        private static final String COL_IU = "InternetUsage";
        private static final String COL_RC = "RCode";
        private static final String COL_DATA_USAGE = "dataUsage";
        private static final String COL_fl = "flag";


        private static ContentValues getLocationItemValues(
                LocationItem locationItem) {

            ContentValues values = new ContentValues();
            values.put(Userlocation.COL_ID, locationItem.getId());
            values.put(Userlocation.COL_POINT_CURRENT_LAT, locationItem.getLat());
            values.put(Userlocation.COL_POINT_CURRENT_LNG, locationItem.getLongi());
            values.put(Userlocation.COL_POINT_DATE_TIME, locationItem.getDatee());
            values.put(Userlocation.COL_DeviceID, locationItem.getDeviceID());
            values.put(Userlocation.COL_Altitude, locationItem.getAltitude());
            values.put(Userlocation.COL_Reason, locationItem.getReason());
            values.put(Userlocation.COL_Speed, locationItem.getSpeed());
            values.put(Userlocation.COL_Direction, locationItem.getDirection());
            values.put(Userlocation.COL_GPSFix, locationItem.getGPSFix());
            values.put(Userlocation.COL_NOS, locationItem.getNOS());
            values.put(Userlocation.COL_HDOP, locationItem.getHDOP());
            values.put(Userlocation.COL_IU, locationItem.getGSMCellID());
            values.put(Userlocation.COL_BS, locationItem.getBatteryPercent());
            values.put(Userlocation.COL_RC, locationItem.getRcode());
            values.put(Userlocation.COL_DATA_USAGE, locationItem.getDataUsage());
            values.put(Userlocation.COL_fl, 1);

            return values;
        }
    }
    private static class Data {
        private static final String TABLE_NAME = "DataHistory";
        private static final String COL_ID = "id";
        private static final String COL_DATA_USED = "DataUsed";
        private static final String COL_DATE = "Date";
//        private static final String COL_DATE = "Date";



        private static ContentValues getDataUsageValues(
                Internet locationItem) {

            ContentValues values = new ContentValues();
            values.put(Data.COL_DATA_USED, locationItem.getData());
            values.put(Data.COL_DATE, locationItem.getDate());

            return values;
        }
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i("ON CREATE DB", "DB");


        sqLiteDatabase.execSQL("CREATE TABLE " + Userlocation.TABLE_NAME + " ("
                + Userlocation.COL_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                // + StatusReasonTable.COL_USER_ID + " VARCHAR ,"
                + Userlocation.COL_POINT_CURRENT_LAT + " TEXT ,"
                + Userlocation.COL_POINT_CURRENT_LNG + " TEXT ,"
                + Userlocation.COL_POINT_DATE_TIME + " TEXT ,"
                + Userlocation.COL_DeviceID+" TEXT ,"
                + Userlocation.COL_Altitude+" TEXT ,"
                + Userlocation.COL_Reason+" TEXT ,"
                + Userlocation.COL_Speed+" TEXT ,"
                + Userlocation.COL_Direction+" TEXT ,"
                + Userlocation.COL_GPSFix+" TEXT ,"
                + Userlocation.COL_NOS+" TEXT ,"
                + Userlocation.COL_HDOP+" TEXT ,"
                + Userlocation.COL_IU+" TEXT ,"
                + Userlocation.COL_BS+" TEXT ,"
                + Userlocation.COL_RC+" TEXT ,"
                + Userlocation.COL_DATA_USAGE+" TEXT ,"
                + Userlocation.COL_fl+" INTEGER "
                +")");
        sqLiteDatabase.execSQL("CREATE TABLE "+ Data.TABLE_NAME+" ( " + Data.COL_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+ Data.COL_DATA_USED+" INTEGER ,"+ Data.COL_DATE+
        " TEXT )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        switch (oldVersion) {
            case 1:

            case 2:


        }
    }

//    public void openDB(boolean read) {
//        if (read)
//            dataBase = this.getReadableDatabase();
//        else
//            dataBase = this.getWritableDatabase();
//    }

//    public void closeDB() {
//        if(sInstance != null) {
//            dataBase.close();
//            dataBase = null;
//        }
//    }
    public long insertInHistoryTable(LocationItem locationItem) throws Exception {

//            openDB(false);
        SQLiteDatabase db = this.getWritableDatabase();
            long id = db.insert(Userlocation.TABLE_NAME, Userlocation.COL_ID,
                    Userlocation.getLocationItemValues(locationItem));

            db.close();
        return  id;
    }

    public long insertDataUseage(Internet internet) throws Exception {

//            openDB(false);
        SQLiteDatabase db = this.getWritableDatabase();
            long id = db.insert(Data.TABLE_NAME,null, Data.getDataUsageValues(internet));

        db.close();
            return id;

    }
    public ArrayList<Internet> getDataUseage(String d){
        ArrayList<Internet> dataUsageArrayList = new ArrayList<Internet>();
        String qry = "SELECT  * FROM " + Data.TABLE_NAME + " WHERE " + Data.COL_DATE + "=?" ;

//            openDB(true);
        SQLiteDatabase db = this.getWritableDatabase();
            String[] args = new String[]{d};
            Cursor cursor = db.rawQuery(qry, args);
//            System.out.println("get Data usage counter : "+cursor.getCount());
            if (cursor.moveToFirst()) {
                do {

                    Internet internetUsage = new Internet();
                    internetUsage.setData(cursor.getInt(1));
                    internetUsage.setDate(cursor.getString(2));
                    //Adding contact to list
                    dataUsageArrayList.add(internetUsage);
                } while (cursor.moveToNext());
            }
        System.out.println("get Data usage called");

            db.close();

        return dataUsageArrayList;
    }


    public ArrayList<LocationItem> getAllPointsFromLog_History() {
        ArrayList<LocationItem> locationItemArrayList = new ArrayList<LocationItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Userlocation.TABLE_NAME+" WHERE "+ Userlocation.COL_fl+" = "+1+" ORDER BY "+ Userlocation.COL_ID +" ASC";// + " WHERE " + StatusReasonTable.COL_RECEIPT_STATUS + "=?";
        // String[] args = new String[]{Constants.ACTIVE};

//            openDB(true);
        SQLiteDatabase db = this.getWritableDatabase();
            //Cursor cursor = dataBase.rawQuery(selectQuery, args);
            Cursor cursor = db.rawQuery(selectQuery, null);
            System.out.println("cursor count : "+cursor.getCount());

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    LocationItem locationItem = new LocationItem();
                    locationItem.setId(cursor.getString(0));
                    locationItem.setLat(cursor.getString(1));
                    locationItem.setLongi(cursor.getString(2));
                    locationItem.setDatee(cursor.getString(3));
                    locationItem.setDeviceID(cursor.getString(4));
                    locationItem.setAltitude(cursor.getString(5));
                    locationItem.setReason(cursor.getString(6));
                    locationItem.setSpeed(cursor.getString(7));
                    locationItem.setDirection(cursor.getString(8));
                    locationItem.setGPSFix(cursor.getString(9));
                    locationItem.setNOS(cursor.getString(10));
                    locationItem.setHDOP(cursor.getString(11));
                    locationItem.setGSMCellID(cursor.getString(12));
                    locationItem.setBatteryPercent(cursor.getInt(13));
                    locationItem.setRcode(cursor.getString(14));
                    locationItem.setDataUsage(cursor.getString(15));


                    //Adding contact to list
                    locationItemArrayList.add(locationItem);
                } while (cursor.moveToNext());
            }

//            cursor.close();
            // return contact list
//            db.close();
            return locationItemArrayList;

    }
    public ArrayList<LocationItem> getAllPointsFromLog_History2() {
        ArrayList<LocationItem> locationItemArrayList = new ArrayList<LocationItem>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Userlocation.TABLE_NAME+" WHERE "+ Userlocation.COL_fl+" = "+1+" ORDER BY "+ Userlocation.COL_ID +" ASC";// + " WHERE " + StatusReasonTable.COL_RECEIPT_STATUS + "=?";
        // String[] args = new String[]{Constants.ACTIVE};
        SQLiteDatabase db = this.getReadableDatabase();
//         openDB(true);
            //Cursor cursor = dataBase.rawQuery(selectQuery, args);
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    LocationItem locationItem = new LocationItem();
                    locationItem.setId(cursor.getString(0));
                    locationItem.setLat(cursor.getString(1));
                    locationItem.setLongi(cursor.getString(2));
                    locationItem.setDatee(cursor.getString(3));
                    locationItem.setDeviceID(cursor.getString(4));
                    locationItem.setAltitude(cursor.getString(5));
                    locationItem.setReason(cursor.getString(6));
                    locationItem.setSpeed(cursor.getString(7));
                    locationItem.setDirection(cursor.getString(8));
                    locationItem.setGPSFix(cursor.getString(9));
                    locationItem.setNOS(cursor.getString(10));
                    locationItem.setHDOP(cursor.getString(11));
                    locationItem.setGSMCellID(cursor.getString(12));
                    locationItem.setBatteryPercent(cursor.getInt(13));
                    locationItem.setRcode(cursor.getString(14));

                    //Adding contact to list
                    locationItemArrayList.add(locationItem);
                } while (cursor.moveToNext());
            }


            db.close();
            return locationItemArrayList;

    }

//    public boolean isLogEmpty_location() {
//
//        String selectQuery = "SELECT  * FROM " + Userlocation.TABLE_NAME;
//        SQLiteDatabase db = this.getReadableDatabase();
////            openDB(false);
//            Cursor cursor = dataBase.rawQuery(selectQuery, null);
//            if (cursor.moveToFirst()) {
//                return false;
//            }
//            cursor.close();
//            return true;
//    }

    public int getTableRowsCount() {
//        String countQuery = "SELECT  * FROM " + Userlocation.TABLE_NAME;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;

        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, Userlocation.TABLE_NAME);
        db.close();
        return (int)count;
    }

//   public void deleteFromHistoryTableWithID(String localID) {
//
//        try {
//            openDB(false);
//            dataBase.delete(Userlocation.TABLE_NAME, Userlocation.COL_ID + "=" + localID, null);
//        } catch (Exception ex) {
//            throw ex;
//        } finally {
////            closeDB();
//        }
//    }

    public int deleteRow(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int numOfEntry = db.delete(Userlocation.TABLE_NAME, Userlocation.COL_ID + " = "+id, null);
        db.close();
        return  numOfEntry;
    }

//    public void updateflag(){
//        try {
//            openDB(false);
//        ContentValues cv = new ContentValues();
//        cv.put(Userlocation.COL_fl, 0);
//        dataBase.update(Userlocation.TABLE_NAME, cv, Userlocation.COL_fl ,null);
//        } catch (Exception ex) {
//            throw ex;
//        } finally {
////            closeDB();
//        }
//    }
    public void updateInternet(Internet internet){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(Data.COL_DATA_USED, internet.getData());
            values.put(Data.COL_DATE, internet.getDate());


            db.update(Data.TABLE_NAME,
                    values,
                    Data.COL_DATE + "=?", new String[]{internet.getDate()});
//            db.close();

        } catch (Exception ex) {
            throw ex;
        } finally {
//            closeDB();
        }

    }


//    public long getLogCount() {
//        try {
//            //String selectQuery = "SELECT Count(*) FROM " + PointsLogTable.TABLE_NAME;
//            openDB(false);
//            // Cursor cursor = dataBase.rawQuery(selectQuery, null);
//            //return cursor.getCount();
//
//            long numRows = DatabaseUtils.queryNumEntries(dataBase, Data.TABLE_NAME);
//            return numRows;
//        } catch (Exception ex) {
//            throw ex;
//        } finally {
////            closeDB();
//        }
//    }


}
