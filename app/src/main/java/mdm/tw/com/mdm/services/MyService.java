package mdm.tw.com.mdm.services;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.TrafficStats;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import mdm.tw.com.mdm.DashboardActivity;
import mdm.tw.com.mdm.db.DbHelper;
import mdm.tw.com.mdm.helpers.Constants;
import mdm.tw.com.mdm.helpers.MyPhoneStateListener;
import mdm.tw.com.mdm.helpers.NetworkChangeReceiver;
import mdm.tw.com.mdm.helpers.NetworkUtil;
import mdm.tw.com.mdm.helpers.PrefrenceUtils;
import mdm.tw.com.mdm.helpers.UtilsFunctions;
import mdm.tw.com.mdm.interfaces.SendHistory;
import mdm.tw.com.mdm.models.LocationItem;
import mdm.tw.com.mdm.models.SimDetailModel;
import mdm.tw.com.mdm.utils.CommonMethods;
import mdm.tw.com.mdm.utils.Constants_Keys;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by developer on 5/22/18.
 */

public class MyService extends Service implements LocationListener, GpsStatus.Listener, GpsStatus.NmeaListener {

//  public int oneMinuteTime = 1 * 60 * 1000;
    public int THIRTY_SECOND_TIME = 30000;

    public int oneMinuteTime = CommonMethods.getIntPreference(MyService.this,Constants.PREF_MDM, Constants.POLL_INTERVAL,1)* 60 * 1000;
    public int distanceCovered = CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED, 500);

    private static final long THIRTY_SEC_TIME = 30 * 1000;
    private Handler mHandler;

    OkHttpClient okHttpClient;
    private Retrofit retrofit;
    SendHistory service;
    public int counter= 0;
    Context context;
    BatteryChangeReceiver batteryChangeReceiver;
    NetworkChangeReceiver networkChangeReceiver;
    TelephonyManager telephonyManager;
    protected LocationManager locationManager;
    // Flag for GPS status
    boolean isGPSEnabled = false;

    // Flag for network status
    boolean isNetworkEnabled = false;

    // Flag for GPS status
    boolean canGetLocation = false;
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    protected String latestHdop="";
    Location location; // Location
    double latitude; // Latitude
    double longitude; // Longitude
    private float distanceInMeters,oldDir  = 0;


    static long internetuseage=0;
    static long mobileData=0;
    double Altitude;
    double Speed; // Longitude
    static double lat,lng;

    DbHelper db;
    private long difer=0;
    private long oldDayData=0;
    private long thistinmeuse=0;

    private boolean charging;
    private boolean flag=true;
    public static boolean firstTime = true;
    public static double distanceCal = 0.0;

    public boolean isGPS = true;

    final static int REQUEST_LOCATION = 199;

    public MyService(Context applicationContext) {
        super();
        this.context = applicationContext;
        Log.i("HERE", "here I am!");
    }
    Criteria criteria;

    public MyService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        db = DbHelper.getInstance(getApplicationContext());
        networkChangeReceiver = new NetworkChangeReceiver();
        UtilsFunctions.registerNetworkReceiver(getApplicationContext(), networkChangeReceiver);
        batteryChangeReceiver = new BatteryChangeReceiver();
        UtilsFunctions.registerBatteryReceiver(getApplicationContext(), batteryChangeReceiver);
        telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
        listenPhoneStateListener();
        registerReceiver(syncDataReceiver, new IntentFilter(
                SyncDataWithServer.SYNC_DATA_NOTIFICATION));

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        criteria.setAltitudeRequired(true);
        criteria.setSpeedRequired(true);
        criteria.setCostAllowed(true);
        criteria.setBearingRequired(true);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);

        if (NetworkUtil.getConnectivityStatus(this) > 0 ) System.out.println("Connect");
        else System.out.println("No connection");
        okHttpClient = UtilsFunctions.getOkHttpClient();
        this.initilizeApi();

        call();
        getLocation(criteria);

        if(DashboardActivity.objGetRecordValue != null) {
            DashboardActivity.objGetRecordValue.showRecordValue(db.getTableRowsCount());
        }

        System.out.println("poll interval : "+CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_INTERVAL, 0));
        System.out.println("poll distance : "+CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED, 0));

        this.mHandler = new Handler();

        this.mHandler.postDelayed(m_Runnable, 60000);

        // waqas - changes
//        this.mHandler.postDelayed(m_Runnable, oneMinuteTime);

    }

    private final Runnable m_Runnable = new Runnable()
    {
        public void run()

        {
//            listenPhoneStateListener();
            System.out.println("Call handler timer : "+ counter);
            if(counter == 5){
                if(NetworkUtil.getConnectivityStatus(getApplicationContext()) != 0) {
                    call();
                }else{
                    System.out.println("Internet not connected");
                }
                counter = 0;

            }

            if(firstTime){
                MyService.firstTime = false;
                getLocation(criteria);
            }else{
                if(location == null){

                    if(PrefrenceUtils.getGoogleLocLatitude(getApplicationContext(), 0) != 0 && PrefrenceUtils.getGoogleLocLongitude(getApplicationContext(), 0) != 0){

                        location = new Location("");
                        location.setLatitude(PrefrenceUtils.getGoogleLocLatitude(getApplicationContext(), 0));
                        location.setLongitude(PrefrenceUtils.getGoogleLocLongitude(getApplicationContext(), 0));
                        System.out.println("Google location : " + "Latitude : "+PrefrenceUtils.getGoogleLocLatitude(getApplicationContext(), 0) + "Longitude : "+PrefrenceUtils.getGoogleLocLongitude(getApplicationContext(), 0));
                    }
                }
            }
//            if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
//               CommonMethods.turnGPSOn(getApplicationContext());
//            }
            save_values("22");
            if(NetworkUtil.getConnectivityStatus(getApplicationContext()) != 0) {
                if(!UtilsFunctions.isServiceRunning(SyncDataWithServer.class, getApplicationContext())) {
                    startService(new Intent(getApplicationContext(), SyncDataWithServer.class));
                }
            }
            counter++;

            MyService.this.mHandler.postDelayed(m_Runnable, oneMinuteTime);
        }

    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        int pollInterval = CommonMethods.getIntPreference(MyService.this,Constants.PREF_MDM, Constants.POLL_INTERVAL,1);
        System.out.println("pollInterval : "+pollInterval);
        oneMinuteTime = pollInterval * 60 * 1000;
        distanceCovered = CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED, 500);

        System.out.println("oneMinuteTime : "+oneMinuteTime);

//        oneMinuteTime = CommonMethods.getIntPreference(MyService.this,Constants.PREF_MDM, Constants.POLL_INTERVAL,1)* 60 * 1000;
        System.out.println("oneMinuteTime : "+ oneMinuteTime);


        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");
        UtilsFunctions.unRegisterNetworkReceiver(getApplicationContext(), networkChangeReceiver);
        UtilsFunctions.unRegisterBatteryStatus(getApplicationContext(), batteryChangeReceiver);
        unregisterReceiver(syncDataReceiver);
        mHandler.removeCallbacks(m_Runnable);
//        db.closeDB();

        Intent broadcastIntent = new Intent("uk.ac.shef.oak.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);

    }


    public void initilizeApi(){

        if((PrefrenceUtils.getPrimaryIP(getApplicationContext(), "").equals(""))){
            if (PrefrenceUtils.getSecondaryIP(getApplicationContext(), "").equals("")){
                retrofit = new Retrofit.Builder()
                        .baseUrl(Constants_Keys.USER_METHODS.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build();
                PrefrenceUtils.savePrimaryIP(getApplicationContext(), "103.8.112.78");
                PrefrenceUtils.saveSecondaryIP(getApplicationContext(), "103.8.112.78");
            }
            else{
                retrofit = new Retrofit.Builder()
                        .baseUrl("http://"+ PrefrenceUtils.getSecondaryIP(getApplicationContext(), "")
                                + Constants_Keys.USER_METHODS.EXTENDED_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build();
            }
        }
        else {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://"+ PrefrenceUtils.getPrimaryIP(getApplicationContext(), "")
                            + Constants_Keys.USER_METHODS.EXTENDED_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }

        service = retrofit.create(SendHistory.class);

    }

    public void call(){

        System.out.println("In method call");
        Call<SimDetailModel> tokenResponseCall = service.getImei(UtilsFunctions.getSid(),"",PrefrenceUtils.getImei(getApplicationContext()),UtilsFunctions.getVersionName(getApplicationContext()),""+UtilsFunctions.getCurrentApiVersion(),UtilsFunctions.getDeviceName());

        tokenResponseCall.enqueue(new Callback<SimDetailModel>() {
            @Override
            public void onResponse(Call<SimDetailModel> call, Response<SimDetailModel> response) {
                int statusCode = response.code();
                Log.d("Response Code", ""+statusCode);
                if (statusCode == 200) {

                    SimDetailModel model = new SimDetailModel();
                    model = response.body();

                    if (model.getIsValid() == false) {
                        CommonMethods.setIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_INTERVAL, 1);
                        CommonMethods.setIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_ANGLE_CHANGE, 5);
                        CommonMethods.setIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED, 500);
                        CommonMethods.setIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_BATTERY_LOW_LEVEL,15);
                        CommonMethods.setStringPreference(MyService.this, Constants.PREF_MDM, Constants.PRIMARY_IP, Constants_Keys.USER_METHODS.primary_ip);
                        CommonMethods.setStringPreference(MyService.this, Constants.PREF_MDM, Constants.SECONDARY_IP, Constants_Keys.USER_METHODS.s_ip);
                    }else {

//                        for (int index = 0; index < model.getResult().size(); index++) {

                        if(model.getResult()!=null) {
                            System.out.println("in service : "+ model.getResult().getPollInterval());
                            System.out.println("in service distance: "+ model.getResult().getPollDistanceCovered());

                            if (CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_INTERVAL, 0) != model.getResult().getPollInterval()
                                    || CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_ANGLE_CHANGE, 0) != model.getResult().getPollAngleChange()
                                    || CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED, 0) != model.getResult().getPollDistanceCovered()
                                    || CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_BATTERY_LOW_LEVEL, 0) != model.getResult().getPollBatteryLowLevel()
                                    || !CommonMethods.getStringPreference(MyService.this, Constants.PREF_MDM, Constants.PRIMARY_IP, "").equalsIgnoreCase(model.getResult().getSecondaryIP())
                                    || !CommonMethods.getStringPreference(MyService.this, Constants.PREF_MDM, Constants.SECONDARY_IP, "").equalsIgnoreCase(model.getResult().getSecondaryIP()))
                            {

                                CommonMethods.setIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_INTERVAL, model.getResult().getPollInterval());
                                CommonMethods.setIntPreference(MyService.this,Constants.PREF_MDM, Constants.POLL_ANGLE_CHANGE, model.getResult().getPollAngleChange());
                                CommonMethods.setIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED, model.getResult().getPollDistanceCovered());
                                CommonMethods.setIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_BATTERY_LOW_LEVEL, model.getResult().getPollBatteryLowLevel());
                                CommonMethods.setStringPreference(MyService.this, Constants.PREF_MDM, Constants.PRIMARY_IP, model.getResult().getPrimaryIP());
                                CommonMethods.setStringPreference(MyService.this, Constants.PREF_MDM, Constants.SECONDARY_IP, model.getResult().getSecondaryIP());
//                                stopService(new Intent(getApplicationContext(), MyService.class));
//                                stopService(new Intent(getApplicationContext(), SyncDataWithServer.class));
//                                startService(new Intent(getApplicationContext(), MyService.class));
//                                startService(new Intent(getApplicationContext(), SyncDataWithServer.class));

                                Log.d("Response Poll Int:", String.valueOf(CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_INTERVAL, 0)));
                                Log.d("Response", String.valueOf(CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_ANGLE_CHANGE, 0)));
                                Log.d("Response", String.valueOf(CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED, 0)));
                            } else {
                                Log.d("Response", CommonMethods.getStringPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_INTERVAL, ""));
                                Log.d("Response", CommonMethods.getStringPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_ANGLE_CHANGE, ""));
                                Log.d("Response", CommonMethods.getStringPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED, ""));

                            }
                        }
                        Log.d("Response", String.valueOf(CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_INTERVAL, 0)));
                        Log.d("Response", String.valueOf(CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_ANGLE_CHANGE, 0)));
                        Log.d("Response", String.valueOf(CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED, 0)));

//                        }

//                        if (model.getResult()!=null && !(model.getResult().getAppVersion().equalsIgnoreCase(versionName))) {
//                            startDownload();
//                        }
                    }
//                    if(firstbool){
//
//                        startService(new Intent(getApplicationContext(), MyService.class));
//                        startService(new Intent(getApplicationContext(), SyncDataWithServer.class));
//                        firstbool=false;
//                    }

                } else {
                    Log.d("Imei","Device Not Found");
                }
            }

            @Override
            public void onFailure(Call<SimDetailModel> call, Throwable t) {
//                if(firstbool){
//
//                    startService(new Intent(getApplicationContext(), MyService.class));
//                    startService(new Intent(getApplicationContext(), SyncDataWithServer.class));
//                    firstbool=false;
//                }
                Log.i("FAIL: ", "ON FAIL: " + t.getMessage());
                //Toast.makeText(SyncDataWithServer.this, "FAILED" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onGpsStatusChanged(int i) {
        System.out.println("onGpsStatusChanged : "+i);
        getLocation(criteria);
    }

    @Override
    public void onLocationChanged(Location location2) {

        System.out.println("get bearings in location : "+location2.getBearing());
        System.out.println("get speed in location : "+location2.getSpeed());
        System.out.println("get Altitude in location : "+(float) location2.getAltitude());

        if(location!=null) {
            distanceInMeters = location.distanceTo(location2);
            distanceCovered = CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_DISTANCE_COVERED,500);
            int valPollAngleChange = CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, Constants.POLL_ANGLE_CHANGE,35);
            int valLastDistanceInMeter = CommonMethods.getIntPreference(MyService.this, Constants.PREF_MDM, "distanceInMeters",0);

            System.out.println("MyService.distanceCal : "+MyService.distanceCal);
            MyService.distanceCal = MyService.distanceCal + distanceInMeters;
            distanceInMeters = valLastDistanceInMeter + distanceInMeters;

            if(DashboardActivity.objGetRecordValue != null){

                DashboardActivity.objGetRecordValue.showAltitudeValues("Altitude is : "+(int)getAltitude());
                DashboardActivity.objGetRecordValue.showSpeedValues(""+getSpeed());
                DashboardActivity.objGetRecordValue.showDirectionValues(""+(int)getDirection());
                DashboardActivity.objGetRecordValue.showDistanceValues(""+(int)MyService.distanceCal);
                DashboardActivity.objGetRecordValue.showLatLngValues("Lat lng is : "+Double.toString(getLatitude())+" "+Double.toString(getLongitude()));

            }
            System.out.println("distanceInMeters : "+ distanceInMeters);
            System.out.println("valPollDistanceCoverd : "+ distanceCovered);

//            if(distanceInMeters != valLastDistanceInMeter) {

                if (MyService.distanceCal > distanceCovered) {
                    MyService.distanceCal = 0.0;
                    save_values("21");
                    CommonMethods.setIntPreference(MyService.this, Constants.PREF_MDM, "distanceInMeters", 0);
                }
//            }


            //waqas - comment the direction
//            if((getDirection()-oldDir)> valPollAngleChange ||
//                    (oldDir-getDirection())> valPollAngleChange){
//
//                save_values("20");
//                oldDir=getDirection();
//
//            }


        }
        location=location2;

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onNmeaReceived(long l, String s) {

        this.setHDOP(s);
    }

    public void setHDOP(String nmeaSentence){
        String[] nmeaParts = nmeaSentence.split(",");

        if (nmeaParts[0].equalsIgnoreCase("$GPGSA")) {

            if (nmeaParts.length > 16 ) {
                this.latestHdop = nmeaParts[16];
            }
        }

        if (nmeaParts[0].equalsIgnoreCase("$GPGGA")) {
            if (nmeaParts.length > 8 ) {
                this.latestHdop = nmeaParts[8];
            }
        }
    }

    public void listenPhoneStateListener(){
        MyPhoneStateListener myPhoneStateListener = new MyPhoneStateListener();
        telephonyManager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    public Location getLocation(Criteria criteria) {


            if ((int) Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    return  getLocationCal(criteria);
                }
            }else{
                return getLocationCal(criteria);

            }

        return getLocationCal(criteria);
    }

    public Location getLocationCal(Criteria criteria){

        try {
            if ((int) Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager.addNmeaListener(this);
                    // Getting GPS status
                    isGPSEnabled = locationManager
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);

                    // Getting network status
                    isNetworkEnabled = locationManager
                            .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                }
            }else{
                locationManager.addNmeaListener(this);
                // Getting GPS status
                isGPSEnabled = locationManager
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);

                // Getting network status
                isNetworkEnabled = locationManager
                        .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            }

                    if (!isGPSEnabled && !isNetworkEnabled) {
                        // No network provider is enabled
                        System.out.println("MyService : No network provider is enabled");

                    } else {
                        this.canGetLocation = true;
                        // If GPS enabled, get latitude/longitude using GPS Services
                        if (isGPSEnabled) {
                            if (location == null) {
//                                locationManager.addGpsStatusListener(this);
//                                locationManager.requestLocationUpdates(1000, 10, criteria, this, null);
                                locationManager.requestLocationUpdates(
                                        LocationManager.GPS_PROVIDER,
                                        30000,
                                        10, this);
                                Log.d("GPS Enabled", "GPS Enabled");
                                if (locationManager != null) {
                                    location = locationManager
                                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                    if (location != null) {
                                        PrefrenceUtils.saveGoogleLocation(getApplicationContext(), location);

                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();
                                        System.out.println("GPS_PROVIDER latitude : " + latitude);
                                        System.out.println("GPS_PROVIDER longitude : " + longitude);
                                    }else{
                                        System.out.println("GPS location == null");
                                    }
                                }else{
                                    System.out.println("GPS locationManager == null");
                                }
                            }else{
                                System.out.println("GPS location != null");
                            }
                        } else if (isNetworkEnabled) {
                            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return null;
                            }
                            locationManager.requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    30000,
                                    10, this);
                            Log.d("Network", "Network");
                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (location != null) {
                                    PrefrenceUtils.saveGoogleLocation(getApplicationContext(), location);
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();

                                    System.out.println("NETWORK_PROVIDER latitude : " + latitude);
                                    System.out.println("NETWORK_PROVIDER longitude : " + longitude);
                                }
                            }
                        }

                    }

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return location;

    }


    public void save_values(String rc){

        System.out.println("Total records : "+db.getTableRowsCount());

        Log.d("test"," "+rc);
        LocationItem point=new LocationItem();

        if(getLatitude()==0)
        {
            point.setLat(""+lat);
        }
        else {
            point.setLat(""+getLatitude());

        }
        if(getLongitude()==0)
        {
            point.setLongi(""+lng);
        }
        else {
            point.setLongi(""+getLongitude());
        }
        point.setDeviceID(PrefrenceUtils.getImei(getApplicationContext()));
        point.setAltitude(""+getAltitude());
        point.setReason("GPS Data");
        point.setSpeed(""+(int)getSpeed());
        point.setDirection(""+(int)getDirection());
        if (getAltitude()!=0.0){
            point.setGPSFix("true");
        }
        else{
            point.setGPSFix("false");
        }
        point.setNOS(""+getNoSatellites());
        point.setHDOP(""+latestHdop);


        try {
            Log.e("bytes recvd", "" + android.net.TrafficStats.getMobileRxBytes());
            Log.e("Total", "Bytes received :" + android.net.TrafficStats.getTotalRxBytes());


            long mobile = TrafficStats.getMobileRxBytes() + TrafficStats.getMobileTxBytes();
            long total = TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes();
            System.out.println("Data Wifi : " + (total - mobile) + " bytes");
            System.out.println("Data Mobile : " + mobile + " bytes");
            System.out.println("Data Total : " + total+ " bytes");

            if(mobile == 0){

                MyService.mobileData = PrefrenceUtils.getMobileByte(getApplicationContext(), MyService.mobileData);
                long wifiData = total - MyService.mobileData;
                point.setDataUsage("wifi="+wifiData +",mobile="+MyService.mobileData);
                System.out.println("wifi="+wifiData +",mobile="+MyService.mobileData);


            }else{
                MyService.mobileData = mobile;

                PrefrenceUtils.saveMobileBytes(getApplicationContext(), mobile);
                long wifiData = total - mobile;
                point.setDataUsage("wifi="+wifiData +",mobile="+mobile);
                System.out.println("wifi="+wifiData +",mobile="+mobile);

//                Date date=new Date();
//                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
//                String dat = dateFormat.format(date);
//
//                ArrayList<Internet> a = db.getDataUseage(dat);
//                if(a.size()>0){
//
//                    Internet internet=new Internet();
//
//                    difer=android.net.TrafficStats.getMobileRxBytes()-internetuseage;
//
//                    internetuseage=android.net.TrafficStats.getMobileRxBytes();
//                    thistinmeuse=  (a.get(0).getData()+difer);
//
//
//                    internet.setData(thistinmeuse);
//                    internet.setDate(dat);
//                    System.out.println("Update Internet Data usage : "+internet.getData());
//                    db.updateInternet(internet);
//                }
//                else{
//                    if(internetuseage!=0&&android.net.TrafficStats.getMobileRxBytes()>internetuseage){
//                        oldDayData=android.net.TrafficStats.getMobileRxBytes()-internetuseage;
//                    }else{
//                        oldDayData=android.net.TrafficStats.getMobileRxBytes();
//                    }
//                    internetuseage=android.net.TrafficStats.getMobileRxBytes();
//                    point.setDataUsage(oldDayData+"");
//                    System.out.println("Internet Usage: "+oldDayData);
//                    Internet internet=new Internet();
//                    internet.setData(oldDayData);
//                    internet.setDate(dat);
//
//                    System.out.println("Internet Data usage : "+internet.getData());
//
//                    db.insertDataUseage(internet);
//                }
            }

            String celli = UtilsFunctions.getCellInfo(getApplicationContext());
            Intent intent;
            int level = getBatteryPercentage(MyService.this);
            Log.e("Total", " Battery :" + level);
            point.setGSMCellID(celli);
            point.setBatteryPercent(level);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//            Date date = new Date(location.getTime());
//            String formatted = sdf.format(date);
            String currentDateandTime = sdf.format(new Date());
            point.setDatee(currentDateandTime);


            System.out.println("Save current date time : "+ currentDateandTime);
            point.setRcode(rc);
            db.insertInHistoryTable(point);
            if(DashboardActivity.objGetRecordValue != null) {
                DashboardActivity.objGetRecordValue.showRecordValue(db.getTableRowsCount());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public int getDirection(){
        float a = 0;
        if (location!=null){
            boolean hasBearings = location.hasBearing();
            a=location.getBearing();
            System.out.println("hasBearings : "+ hasBearings+ " ,Location : " + a);
        }
        return (int)a;
    }

    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
            lat = latitude;
        }
        else {
            latitude = lat;
        }
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
            lng = longitude;
        }
        else {
            longitude = lng;
        }
        return longitude;
    }

    public int getAltitude(){
        if(location != null){
            Altitude=0.0;
            boolean hasaltitude = location.hasAltitude();
            double altitude = location.getAltitude();
            System.out.println("HasAltitude" + hasaltitude+"-"+altitude);
            Altitude = location.getAltitude();
            if(location.getExtras() != null) {
                location.getExtras().getInt("satellites");
            }
        }
        return (int)Altitude;
    }
    public int getSpeed(){
        double kmphSpeed = 0;
        if(location != null){
            boolean hasSpeed = location.hasSpeed();
//            double altitude = location.getSpeed();

            Speed = location.getSpeed();
            double currentSpeed = round(Speed,3,BigDecimal.ROUND_HALF_UP);
            kmphSpeed = round((currentSpeed*3.6),3, BigDecimal.ROUND_HALF_UP);
            System.out.println("Speed" + hasSpeed+"-"+Speed);
        }

        // return latitude
        return (int)kmphSpeed;
    }

    public static double round(double unrounded, int precision, int roundingMode) {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }

    public int getNoSatellites(){
        int a=0;
        if (location!=null){
            if(location.getExtras() != null) {
                a = location.getExtras().getInt("satellites");
            }
        }
        return a;
    }


    public int getBatteryPercentage(Context context) {
        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean bCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;
        if(bCharging){
            charging=true;
            flag=true;
            Log.d("charging:","true");
        }
        else{

            Log.d("charging:","flase");
            charging=false;
        }
        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;
//       if(CommonMethods.getIntPreference(MyService.this,"MyPref","PollBatteryLowLevel",0)>=(batteryPct*100)){
//           save_values("7");
//        }
        return (int) (batteryPct * 100);
    }

    private BroadcastReceiver syncDataReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            System.out.println("syncDataReceiver called");
            int value=intent.getIntExtra("count", 0);
            if(DashboardActivity.objGetRecordValue != null) {
                DashboardActivity.objGetRecordValue.showRecordValue(db.getTableRowsCount());
            }
        }
    };

}
