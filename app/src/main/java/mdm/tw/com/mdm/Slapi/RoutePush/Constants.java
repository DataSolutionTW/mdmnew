package mdm.tw.com.mdm.Slapi.RoutePush;

public class Constants {
	//public final static String PRIMARY_SERVER_IP_ADDRESS = "192.168.0.147";
	//public final static String PRIMARY_SERVER_IP_ADDRESS = "202.166.174.69";

	public final static String PRIMARY_SERVER_IP_ADDRESS = "203.175.74.153";


	//public final static String SECONDARY_SERVER_IP_ADDRESS = "203.175.74.148";
	//public final static String SECONDARY_SERVER_IP_ADDRESS = "192.168.0.184";//local server for testing
	//public final static String SECONDARY_SERVER_IP_ADDRESS = "203.175.74.153";//local server for testing

	public final static String SECONDARY_SERVER_IP_ADDRESS = "10.1.251.132";
	 //local server for testing

	//public final static String PRIMARY_SERVER_IP_ADDRESS = "192.168.0.184";

	public final static int SERVER_PORT = 6010;
	public final static int TEST_SERVER_PORT = 6011;

	public final static boolean seeToasts = false; // ALERTS
	public final static boolean seeDialogs = false;

	// Bring IGO application to front on Route Proceed.
	public final static boolean showIGO = true;

	public final static String TYPE_COMMAND = "1";
	public final static String TYPE_GPS = "2";
	public final static String TYPE_PING = "3";


	public final static String LOGIN_CODE = "01";
	public final static String PING_CODE = "02";
	public final static String GPS_CODE = "03";

	public final static int SERVER_THREAD_TIME = 30000;
	public final static int GPS_THREAD_TIME = 30000;
	public final static int PING_THREAD_TIME = 10000;
	public final static int SOCKET_TIME_OUT = 3000000 ;

	public final static int LOG_POINTS_LIMIT = 1;
	public final static int RETRY_COUNT = 8;



	public final static String SET_DESTINATION_POSITION = "SDP";
	public final static String ADD_FAV_POINT = "AFP";
	public final static String DELETE_ROUTE = "DR";
	public final static String SET_PLAN_MODE = "SPM";
	public final static String SET_VEHICLE_TYPE = "SVT";
	public final static String SET_LANGUAGE = "SL";
	public final static String SET_SOUND = "SS";
	public final static String SHOW_HIDE_IGO = "SHIG";
	public final static String ADD_WAY_POINT = "AWP";
	public final static String GET_GPS_STATUS = "GGS";
	public static final String GET_ROUTE_INFO = "GRI";
	public static final String GET_LANGUAGE = "GL";

	public final static String DATA_FROM_DEVICE= "$$";
	public final static String DATA_FROM_SERVER= "##";


	public static final String RESPONSE = "RSP";

	public static final String DATE_TIME = "DateTime";
	public static final String TIME_DIFF = "TimeDiff";

	public static final String USER_DETAILS = "USER_IDENTIFIER";
	public static final String NAV_CID = "NAV_CID";
	public static final String REQUEST_FOR_CALL = "RFC";
	public static final String SLAPPI_LICENSE_FILE = "SLF";
}
