package mdm.tw.com.mdm.Slapi;

/**
 * Created by DaNii on 11/17/16.
 */

public class RequestItem {
    private String localID;
    private String requestID;
    private String functionName;
    private String parameters;

    public RequestItem(String requestID, String functionName, String parameters) {
        this.requestID = requestID;
        this.functionName = functionName;
        this.parameters = parameters;
    }

    public String getLocalID() {
        return localID;
    }

    public void setLocalID(String localID) {
        this.localID = localID;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }
}
