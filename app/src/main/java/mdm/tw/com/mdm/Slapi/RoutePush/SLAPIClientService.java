package mdm.tw.com.mdm.Slapi.RoutePush;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mdm.tw.com.mdm.BuildConfig;
import mdm.tw.com.mdm.DashboardActivity;
import mdm.tw.com.mdm.Slapi.CommonMethods;
import mdm.tw.com.mdm.Slapi.LogItem;

//import android.widget.Toast;

//@SuppressWarnings("unused")
public class SLAPIClientService extends Service {

    private IncomingHandler mIncomingHandler = null;
    private Messenger mMessenger = null;
    private static SLAPITransmitter mTransmitter = null;

    private ServerHelloBroadcastReceiver mHelloReceiver = null;
    private ServerByeBroadcastReceiver mByeReceiver = null;

    private Socket mSocket = null;
    private MyClientTask mClientTask = null;

    private AlertDialog dialog;
    private float latitude = 0.0F, longitude = 0.0F;

    int planModeFromServer = 0;

    String functionCallCode = "00";

    private Handler serverHandler;
    private Thread serverThread;

    private Handler gpsHandler;
    private Thread gpsThread;

    private Handler pingHandler;
    private Thread pingThread;




    boolean isFromGpsWorkerThread = false;

    long lastGPSTime;
    long timeDiffFromGPS_device;
    long actualTime;
    String actualDateString;

    String navCID = "";

    private static int PRIMARY_SERVER_RETRY_COUNTER = 0;
    private static int SECONDARY_SERVER_RETRY_COUNTER = 0;

    //private String CURRENT_SERVER_IP = Constants.PRIMARY_SERVER_IP_ADDRESS;
    private String CURRENT_SERVER_IP = Constants.SECONDARY_SERVER_IP_ADDRESS;
    private int CURRENT_SERVER_PPORT = Constants.SERVER_PORT;


    int catchCounter = 0;



    CountDownTimer countDownTimer;


    @Override
    public void onCreate() {
        super.onCreate();
        getNavCID();

        navCID = CommonMethods.getStringPreference(getApplicationContext(),
                Constants.USER_DETAILS, Constants.NAV_CID, "NOT AVAILABLE");

    }

    public void connectSlappi() {
        mIncomingHandler = new IncomingHandler();
        mIncomingHandler.client = this;
        mMessenger = new Messenger(mIncomingHandler);
        mTransmitter = new SLAPITransmitter(this, mMessenger);

        mHelloReceiver = new ServerHelloBroadcastReceiver();
        mByeReceiver = new ServerByeBroadcastReceiver();

        registerReceiver(mHelloReceiver, new IntentFilter(
                "com.navngo.igo.javaclient.SLAPI_SERVER_HELLO"));
        registerReceiver(mByeReceiver, new IntentFilter(
                "com.navngo.igo.javaclient.SLAPI_SERVER_BYE"));

        mTransmitter.doBindService();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, DashboardActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                //.setSmallIcon(R.mipmap.ic_launcher)
                //.setContentTitle("Tracking World Helper")
                //.setContentText("Doing some work...")
                .setContentIntent(pendingIntent).build();

        startForeground(1337, notification);

        mClientTask = (MyClientTask) new MyClientTask(
                CURRENT_SERVER_IP, CURRENT_SERVER_PPORT)
                .execute();


        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).processName.equals("com.navngo.igo.javaclient")) {
                Toast.makeText(this, "IGO is running", Toast.LENGTH_SHORT).show();
                isiGO_Started = true;
                try {
                    mIncomingHandler = new IncomingHandler();
                    mIncomingHandler.client = this;
                    mMessenger = new Messenger(mIncomingHandler);
                    mTransmitter = new SLAPITransmitter(this, mMessenger);

                    mHelloReceiver = new ServerHelloBroadcastReceiver();
                    mByeReceiver = new ServerByeBroadcastReceiver();

                    registerReceiver(mHelloReceiver, new IntentFilter(
                            "com.navngo.igo.javaclient.SLAPI_SERVER_HELLO"));
                    registerReceiver(mByeReceiver, new IntentFilter(
                            "com.navngo.igo.javaclient.SLAPI_SERVER_BYE"));

                    mTransmitter.doBindService();
                    //dir1.createNewFile();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Fail to create", Toast.LENGTH_SHORT).show();
                }
                // gpsWorkerThread();
                serverWorkerThread();
                Intent serviceIntent = new Intent(this, SLAPIClientService.class);
                startService(serviceIntent);
                //finish();
                //ExitActivity.exitApplication(MainActivity.this);
            } else {
                Toast.makeText(this, "IGO is NOT running", Toast.LENGTH_SHORT).show();
                gpsWorkerThread();
                //if (!dbHelper.isLogEmpty()) {
                serverWorkerThread();
                //}

            }
        }



        File dir = new File(Environment.getExternalStorageDirectory() + "/iGO");
        if (dir.exists() && dir.isDirectory()) {
            // do something here
            Toast.makeText(context, "Exist", Toast.LENGTH_SHORT).show();
            //File dir1 = new File(Environment.getExternalStorageDirectory() + "/iGO/license/test.txt");
            try {
                mIncomingHandler = new IncomingHandler();
                mIncomingHandler.client = this;
                mMessenger = new Messenger(mIncomingHandler);
                mTransmitter = new SLAPITransmitter(this, mMessenger);

                mHelloReceiver = new ServerHelloBroadcastReceiver();
                mByeReceiver = new ServerByeBroadcastReceiver();

                registerReceiver(mHelloReceiver, new IntentFilter(
                        "com.navngo.igo.javaclient.SLAPI_SERVER_HELLO"));
                registerReceiver(mByeReceiver, new IntentFilter(
                        "com.navngo.igo.javaclient.SLAPI_SERVER_BYE"));

                mTransmitter.doBindService();
                //dir1.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, "Fail to create", Toast.LENGTH_SHORT).show();
            }
        } else {
            mIncomingHandler = new IncomingHandler();
            mIncomingHandler.client = this;
            mMessenger = new Messenger(mIncomingHandler);
            mTransmitter = new SLAPITransmitter(this, mMessenger);
            Toast.makeText(context, "Not Exist", Toast.LENGTH_SHORT).show();
            mClientTask = (MyClientTask) new MyClientTask(
                    CURRENT_SERVER_IP, CURRENT_SERVER_PPORT)
                    .execute();
        }


        navCID = CommonMethods.getStringPreference(getApplicationContext(),
                Constants.USER_DETAILS, Constants.NAV_CID, "NOT AVAILABLE");


        try {
            if (mSocket.isConnected() && intent.getStringExtra("data").equals("RequestForCall")) {
                String requestForCallStr = Constants.REQUEST_FOR_CALL + "," + "00"
                        + "," + navCID + "||" + 0 + "," + 0;

                sendDataOverSocket(requestForCallStr);
                //Toast.makeText(context, "Request sent successfully, Please wait for the response.", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            e.printStackTrace();
            if(intent.getStringExtra("data").equals("RequestForCall")){
                Toast.makeText(context, "Error sending Request, Please check your connection", Toast.LENGTH_SHORT).show();

            }
        }


        return START_STICKY;
    }

    public void clientFirstHandshake(boolean isFirstClient) {
        if (isFirstClient)
            mTransmitter.sendRequest(SLAPIMessageIds.SLAPI_CLIENT_BYE);

        mTransmitter.sendRequest(SLAPIMessageIds.SLAPI_CLIENT_HELLO);

    }

    @Override
    public void onDestroy() {
        mTransmitter.doUnbindService();

        if (mSocket != null)
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        if (mHelloReceiver != null)
            unregisterReceiver(mHelloReceiver);
        if (mByeReceiver != null)
            unregisterReceiver(mByeReceiver);

        if (mClientTask != null)
            mClientTask.cancel(true);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }


    private class ServerHelloBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            clientFirstHandshake(true);
        }
    }

    private class ServerByeBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            showMessege("SERVER_BYE", "Broadcast received.");
        }
    }

    /**
     * Handles the messages coming from the service
     */
    @SuppressLint("HandlerLeak")
    class IncomingHandler extends Handler {
        public SLAPIClientService client = null;

        @Override
        public void handleMessage(Message msg) {
            if (client != null)
                client.handleThisMessage(msg);
        }
    }

    /*** Displays the message received from the SLAPI server
     */
    protected void handleThisMessage(Message msg) {
        // show//Toast("Handle: " + SLAPIMessageIds.MessageName(msg.what));
        Bundle d = msg.getData();


        switch (msg.what) {
            case SLAPIMessageIds.SLAPI_SERVER_HELLO:
                showMessege(SLAPIMessageIds.MessageName(msg.what), "arg1: "
                        + msg.arg1 + "\n" + "arg2: " + msg.arg2 + "\n"
                        + "HashCode2: " + msg.hashCode());

                if (msg.arg2 == 1) {

                    if (mSocket != null && mSocket.isConnected()) {


                    } else {
                        // AsyncTask to Open Socket Connection.
                        /*mClientTask = (MyClientTask) new MyClientTask(
                                CURRENT_SERVER_IP, CURRENT_SERVER_PPORT)
                                .execute();*/
                    }

                    //gpsWorkerThread();




				/*mTransmitter
                        .sendRequest(SLAPIMessageIds.SLAPI_GETGPSSTATUS);*/


				/*float latitude = Float.valueOf("31.517077");
                float longitude = Float.valueOf("74.330457");

				setDestinationPosition(latitude, longitude);*/
                }
                break;


            case SLAPIMessageIds.SLAPI_OUT_GPSSTATUS:
                //int rID = 0;
                //////Toast.makeText(context, "RID:"+ rID, //Toast.LENGTH_SHORT).show();
                //////Toast.makeText(this, d.getFloat("Latitude") + "\n" + "Longitude:"+ d.getFloat("Longitude"), //Toast.LENGTH_SHORT).show();

                String gps_str = "";
                try {
                    String gpsFix = Data2String.gpsFixValue(d.getInt("GPSFix"));
                    if (!gpsFix.contains("No")) {
                        gpsFix = "F";
                    } else {
                        gpsFix = "N";
                    }

                    gps_str = gpsFix + "," +
                            d.getInt("NofSatellitesTracked") + "," +
                            d.getInt("NofSatellitesinView") + "," +
                            d.getInt("HDOP") + "," +
                            Data2String.utcTime(d) + "," +
                            d.getFloat("Latitude") + "," +
                            d.getFloat("Longitude") + "," +
                            d.getInt("Altitude") + "," +
                            d.getInt("Course") + "," +
                            d.getInt("Speed");
                } catch (Exception e) {
                    //Toast.makeText(context, "Failed to parse GPS RESPONSE", Toast.LENGTH_SHORT).show();
                }


                /*if (mSocket.isConnected()) {
                    try {
                        DataOutputStream output_to_server = new DataOutputStream(mSocket.getOutputStream());
                        output_to_server.writeBytes("RID: "+rID+" "+client_str);
                        output_to_server.flush();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }*/


                try {
                    // Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(CommonMethods.getTodaysDate());
                    Date date = new SimpleDateFormat("yyyy-MM-dd h:m:s").parse(Data2String.utcTime(d));
                    long gpsTime = date.getTime();

                    //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd h:m:s");

                    //String actualGPS_DATE_TIME_String = formatter.format(new Date(gpsTime));


                    long sysTime = System.currentTimeMillis();


                    ////Toast.makeText(context, "System time: " + sysTime, //Toast.LENGTH_SHORT).show();

                    long nowPlus5Minutes = gpsTime + TimeUnit.HOURS.toMillis(5);

                    lastGPSTime = nowPlus5Minutes;

                    timeDiffFromGPS_device = lastGPSTime - sysTime;

                    CommonMethods.setLongPreference(context,
                            Constants.DATE_TIME, Constants.TIME_DIFF, timeDiffFromGPS_device);

                    actualTime = sysTime + timeDiffFromGPS_device;

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd h:m:s");

                    actualDateString = formatter.format(new Date(actualTime));


                    if (isFromGpsWorkerThread) {
                        isFromGpsWorkerThread = false;
                        LogItem logItem = new LogItem();
                        logItem.setType(Constants.TYPE_GPS);
                        logItem.setData(gps_str);
                        logItem.setDateTime(actualDateString);


                    } else {


                        LogItem logItem = new LogItem();
                        logItem.setType(Constants.TYPE_COMMAND);
                        logItem.setData(gps_str);
                        logItem.setDateTime(actualDateString);


                    }

                    //////Toast.makeText(context, "After Conversion :"+dateString, //Toast.LENGTH_SHORT).show();

                    /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
                    String currentDateandTime = sdf.format(new Date());

                    Date date = formatter.parse(currentDateandTime);*/
                    /*Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.HOUR, 5);*/


                    //   System.out.println("Time here "+calendar.getTime());
                    //  ////Toast.makeText(context, "Time Date after conversion: "+calendar.getTime(), //Toast.LENGTH_SHORT).show();


                } catch (ParseException e) {
                    e.printStackTrace();
                }




                /*////Toast.makeText(this, "GPSFix:"
                        + Data2String.gpsFixValue(d.getInt("GPSFix")) + "\n"
                        + "Satellites tracked:" + d.getInt("NofSatellitesTracked")
                        + "\n" + "Satellites in view:"
                        + d.getInt("NofSatellitesinView") + "\n" + "HDOP:"
                        + d.getInt("HDOP") + "\n" + "UTCTime:"
                        + Data2String.utcTime(d) + "\n" + "Latitude:"
                        + d.getFloat("Latitude") + "\n" + "Longitude:"
                        + d.getFloat("Longitude") + "\n" + "Altitude:"
                        + d.getInt("Altitude") + "\n" + "Course:"
                        + d.getInt("Course") + "\n" + "Speed:" + d.getInt("Speed")
                        + "\n", //Toast.LENGTH_LONG).show();

                ////Toast.makeText(context, "Device Date: " + CommonMethods.getTodaysDate(), //Toast.LENGTH_SHORT).show();*/



                /*Date date = new Date(CommonMethods.getTodaysDate());
                Date date1 = new Date(Data2String.utcTime(d));

                //date.toString();

                ////Toast.makeText(context, "Date 1: "+date.toString(), //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(context, "Date 2: "+date1.toString(), //Toast.LENGTH_SHORT).show();*/

                /*showServerMessage(SLAPIMessageIds.MessageName(msg.what), "GPSFix:"
                        + Data2String.gpsFixValue(d.getInt("GPSFix")) + "\n"
						+ "Satellites tracked:" + d.getInt("NofSatellitesTracked")
						+ "\n" + "Satellites in view:"
						+ d.getInt("NofSatellitesinView") + "\n" + "HDOP:"
						+ d.getInt("HDOP") + "\n" + "UTCTime:"
						+ Data2String.utcTime(d) + "\n" + "Latitude:"
						+ d.getFloat("Latitude") + "\n" + "Longitude:"
						+ d.getFloat("Longitude") + "\n" + "Altitude:"
						+ d.getInt("Altitude") + "\n" + "Course:"
						+ d.getInt("Course") + "\n" + "Speed:" + d.getInt("Speed")
						+ "\n");*/

                break;
            /*case SLAPIMessageIds.SLAPI_DELETEROUTE:
                ////Toast.makeText(this, "in DELETE ROUTE", //Toast.LENGTH_SHORT).show();
                break;*/
            /*case SLAPIMessageIds.SLAPI_OUT_ADDRESSFROMPOS:

                final String address = d.getString("Address").trim();
                showRouteDialog(address);
                break;*/

            case SLAPIMessageIds.SLAPI_OUT_ROUTEINFO:
                ////Toast.makeText(this, "IN ROUTE INFO: ", //Toast.LENGTH_SHORT).show();
                float[] waypoints = d.getFloatArray("WayPoints");
                /*String info = "PlanMode:" + Data2String.planMode(d.getInt("PlanMode"))
                        + "\n" + "Number of Waypoints:" + waypoints.length/4;*/

                String info = "";
                info = d.getInt("PlanMode") + "," + waypoints.length / 4 + ",";

                for (int i = 0; i < waypoints.length; i += 4) {
                    info += "[" + waypoints[i + 0] + "," + waypoints[i + 1] + "," + waypoints[i + 2] + "," + waypoints[i + 3];
                }
                /*final String routeInfo = "Plan Mode:"
                        + d.getChar("PlanMode")
                        + "\n" + "No of Waypoints:" + d.getShort("Numberofwaypoints")
                        + "\n" + "Waypoint Latitude:" + d.getFloat("WaypointLatitude")
                        + "\n" + "Waypoint Longitude:" + d.getFloat("WaypointLongitude")
                        + "\n" + "Waypoint Distance:" + d.getInt("Waypointdistance")
                        + "\n" + "Waypoint ETA:" + d.getInt("WaypointETA");*/


                //////Toast.makeText(this, "ROUTE INFO: " + info, //Toast.LENGTH_SHORT).show();


                if (!info.equals("")) {
                    String routeInfo_str = Constants.GET_ROUTE_INFO + "," + functionCallCode
                            + "," + navCID + "||" + info;
                    sendDataOverSocket(routeInfo_str);
                } else {
                    String routeInfo_str = Constants.GET_ROUTE_INFO + "," + functionCallCode
                            + "," + navCID + "||" + 0 + "," + 0;
                    sendDataOverSocket(routeInfo_str);
                }


                break;

            case SLAPIMessageIds.SLAPI_OUT_LANGUAGESET:

                ////Toast.makeText(this, "LANGUAGE: " + Data2String.errorCode(d.getInt("LCID")), //Toast.LENGTH_SHORT).show();

                String languageSet_str = Constants.SET_LANGUAGE + "," + functionCallCode + "," + navCID + "||" + msg.arg1 + "," + msg.arg2;

                sendDataOverSocket(languageSet_str);

                break;

            case SLAPIMessageIds.SLAPI_OUT_LANGUAGE:

                //////Toast.makeText(this, "LANGUAGE: "d.getInt(), //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 1: " + msg.arg1, //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 2: " + msg.arg2, //Toast.LENGTH_SHORT).show();

                //String navCID = CommonMethods.getStringPreference(getApplicationContext(), Constants.USER_DETAILS, Constants.NAV_CID, "NOT AVAILABLE");


                String language_str = Constants.GET_LANGUAGE + "," + functionCallCode + ","
                        + navCID + "||" + msg.arg1 + ",0";


                sendDataOverSocket(language_str);

                break;

            case SLAPIMessageIds.SLAPI_OUT_DESTINATIONSET:

			/*final String routeInfo = "Plan Mode:"
                        + d.getChar("Plan Mode") + "\n" +"Waypoint Latitude:"
						+ d.getFloat("WaypointLatitude") + "\n" + "Waypoint Longitude:"
						+ d.getFloat("WaypointLongitude") ;*/

                //////Toast.makeText(this, "LANGUAGE: "d.getInt(), //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 1: " + msg.arg1, //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 2: " + msg.arg2, //Toast.LENGTH_SHORT).show();


                String destinationSet_str = Constants.SET_DESTINATION_POSITION + "," + functionCallCode + ","
                        + navCID + "||" + msg.arg1 + "," + msg.arg2;


                sendDataOverSocket(destinationSet_str);
                break;

            case SLAPIMessageIds.SLAPI_OUT_WAYPOINTSET:

			/*final String routeInfo = "Plan Mode:"
                        + d.getChar("Plan Mode") + "\n" +"Waypoint Latitude:"
						+ d.getFloat("WaypointLatitude") + "\n" + "Waypoint Longitude:"
						+ d.getFloat("WaypointLongitude") ;*/

                //////Toast.makeText(this, "LANGUAGE: "d.getInt(), //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 1: " + msg.arg1, //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 2: " + msg.arg2, //Toast.LENGTH_SHORT).show();

                String wayPointSet_str = Constants.ADD_WAY_POINT + "," + functionCallCode + ","
                        + navCID + "||" + msg.arg1 + "," + msg.arg2;


                sendDataOverSocket(wayPointSet_str);

                break;

            case SLAPIMessageIds.SLAPI_OUT_VEHICLESET:

			/*final String routeInfo = "Plan Mode:"
                        + d.getChar("Plan Mode") + "\n" +"Waypoint Latitude:"
						+ d.getFloat("WaypointLatitude") + "\n" + "Waypoint Longitude:"
						+ d.getFloat("WaypointLongitude") ;*/

                //////Toast.makeText(this, "LANGUAGE: "d.getInt(), //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 1: " + msg.arg1, //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 2: " + msg.arg2, //Toast.LENGTH_SHORT).show();

                String vehicleSet_str = Constants.SET_VEHICLE_TYPE + "," + functionCallCode + ","
                        + navCID + "||" + msg.arg1 + "," + msg.arg2;


                sendDataOverSocket(vehicleSet_str);

                break;

            case SLAPIMessageIds.SLAPI_OUT_PLANMODESET:

			/*final String routeInfo = "Plan Mode:"
                        + d.getChar("Plan Mode") + "\n" +"Waypoint Latitude:"
						+ d.getFloat("WaypointLatitude") + "\n" + "Waypoint Longitude:"
						+ d.getFloat("WaypointLongitude") ;*/

                //////Toast.makeText(this, "LANGUAGE: "d.getInt(), //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 1: " + msg.arg1, //Toast.LENGTH_SHORT).show();
                ////Toast.makeText(this, "ARG 2: " + msg.arg2, //Toast.LENGTH_SHORT).show();

                String planModeSet_str = Constants.SET_PLAN_MODE + "," + functionCallCode + ","
                        + navCID + "||" + msg.arg1 + "," + msg.arg2;


                sendDataOverSocket(planModeSet_str);

                break;


            default:
                break;
        }

    }

    public class MyClientTask extends AsyncTask<Object, Object, Object> {
        // Socket socket = null;
        String dstAddress;
        int dstPort;

        MyClientTask(String addr, int port) {
            dstAddress = addr;
            dstPort = port;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Object doInBackground(Object... params) {
            String response = "";

            //TODO
            //set delay in retry
            //set hold retry thread on both server after x retries

            //for (int retries = 0; retries < Constants.RETRY_COUNT; retries++) {
            try {

                //Toast.makeText(context, "retr //Toast.LENGTH_SHORT).show();
                mSocket = new Socket(dstAddress, dstPort);
                /*this.socket = mSocket;

                this.socket.setKeepAlive(true);
                this.socket.setSoTimeout(Constants.SOCKET_TIME_OUT);*/

                mSocket.setKeepAlive(true);
                mSocket.setSoTimeout(Constants.SOCKET_TIME_OUT);
                //break;
            } catch (UnknownHostException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            }
            //}

            try {
                if (mSocket.isConnected()) {

                    String versionRelease = Build.VERSION.RELEASE;

                    //String login_str = Constants.LOGIN_CODE + "," + 00 + "," + navCID + "||" + actualDateString + "," + BuildConfig.VERSION_NAME + "," + versionRelease;
                    String login_str = Constants.LOGIN_CODE + "," + "00" + "," + navCID + "||"/* + actualDateString + ","*/ + BuildConfig.VERSION_NAME + "," + versionRelease;

                    sendDataOverSocket(login_str);


                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
                            1024);
                    byte[] buffer = new byte[1024];

                    int bytesRead;
                    InputStream inputStream = mSocket.getInputStream();

					/*
                     * notice: inputStream.read() will block if no data return
					 */
                    while ((bytesRead = inputStream.read(buffer)) > 0) {
                        byteArrayOutputStream.write(buffer, 0, bytesRead);
                        response = byteArrayOutputStream.toString("UTF-8");
                        byteArrayOutputStream.reset();

                        publishProgress(response);
                    }
                } else {
                    // ////Toast.makeText(context, "makeConnectionRequest", //Toast.LENGTH_SHORT).show();
                    makeConnectionRequest();
                }

            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                response = "UnknownHostException: " + e.toString();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                response = "IOException: " + e.toString();
            } finally {
                if (mSocket != null) {
                    try {
                        mSocket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return response;
        }

        private void makeConnectionRequest() {

        }

        @Override
        protected void onProgressUpdate(Object... values) {
            String response = (String) values[0];

            if (response.equalsIgnoreCase("Welcome")
                    || response.equalsIgnoreCase("ping")
                    || response.contains("Welcome")
                    || response.contains("ping")) {
                // Do Nothing

                if (response.equalsIgnoreCase("Welcome")
                        || response.contains("Welcome")) {

                    catchCounter = 0;
                }

            } else {


                String[] separated = response.split(",");


                switch (separated[1].trim()) {

                    case Constants.SET_DESTINATION_POSITION:

                        /*RequestItem requestItem = new RequestItem();
                        requestItem.*/

                        functionCallCode = (separated[2].trim());
                        float latitude = Float.valueOf(separated[3].trim());
                        float longitude = Float.valueOf(separated[4].trim());
                        planModeFromServer = Integer.valueOf(separated[5].trim());


                        mTransmitter.sendRequest(SLAPIMessageIds.SLAPI_DELETEROUTE);

                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("PlanMode", planModeFromServer);
                        resultIntent.putExtra("Latitude", latitude);
                        resultIntent.putExtra("Longitude", longitude);

                        Message msg = Message.obtain(null,
                                SLAPIMessageIds.SLAPI_SETDESTINATIONPOS);
                        msg.setData(resultIntent.getExtras());

                        // Show new route with set destination.
                        mTransmitter.sendRequest(msg);

                        if (Constants.showIGO) {
                            Intent LaunchIntent = getPackageManager()
                                    .getLaunchIntentForPackage(
                                            "com.navngo.igo.javaclient");
                            startActivity(LaunchIntent);
                        }
                        break;

                    case Constants.ADD_FAV_POINT:
                        functionCallCode = (separated[2].trim());
                        Intent resultIntent1 = new Intent();
                        resultIntent1.putExtra("Name", separated[3]);
                        resultIntent1.putExtra("Latitude", Float.valueOf(separated[4].trim()));
                        resultIntent1.putExtra("Longitude", Float.valueOf(separated[5].trim()));



                        Message msg1 = Message.obtain(null,
                                SLAPIMessageIds.SLAPI_ADDFAVORITEPOS);
                        msg1.setData(resultIntent1.getExtras());

                        // Show new route with set destination.
                        mTransmitter.sendRequest(msg1);

                        String addFav_str = Constants.ADD_FAV_POINT + "," + functionCallCode
                                + "," + navCID + "||" + 1 + "," + 0;
                        sendDataOverSocket(addFav_str);

                        break;

                    case Constants.DELETE_ROUTE:
                        functionCallCode = (separated[2].trim());


                        mTransmitter
                                .sendRequest(SLAPIMessageIds.SLAPI_DELETEROUTE);

                        String deleteRoute_str = Constants.DELETE_ROUTE + "," + functionCallCode
                                + "," + navCID + "||" + 1 + "," + 0;
                        sendDataOverSocket(deleteRoute_str);

                        break;

                    case Constants.GET_GPS_STATUS:
                        functionCallCode = (separated[2].trim());



                        isFromGpsWorkerThread = false;
                        mTransmitter
                                .sendRequest(SLAPIMessageIds.SLAPI_GETGPSSTATUS, functionCallCode);

                        String gpsAck_str = Constants.GET_GPS_STATUS + "," + functionCallCode + ","
                                + navCID + "||" + 1 + "," + 0;
                        sendDataOverSocket(gpsAck_str);

                        break;


                    case Constants.SET_PLAN_MODE:
                        functionCallCode = (separated[2].trim());


                        mTransmitter
                                .sendRequest(SLAPIMessageIds.SLAPI_SETPLANMODE,
                                        Integer.parseInt(separated[3]));
                        break;

                    case Constants.SET_VEHICLE_TYPE:
                        functionCallCode = (separated[2].trim());

                        mTransmitter
                                .sendRequest(SLAPIMessageIds.SLAPI_SETVEHICLE,
                                        Integer.parseInt(separated[3]));
                        break;

                    case Constants.SET_LANGUAGE:
                        functionCallCode = (separated[2].trim());


                        mTransmitter
                                .sendRequest(SLAPIMessageIds.SLAPI_SETLANGUAGE,
                                        Integer.parseInt(separated[3]));
                        break;

                    case Constants.SHOW_HIDE_IGO:
                        functionCallCode = (separated[2].trim());

                        mTransmitter
                                .sendRequest(SLAPIMessageIds.SLAPI_SHOWHMI,
                                        Integer.parseInt(separated[3]));

                        String showHide_str = Constants.SHOW_HIDE_IGO + "," + functionCallCode
                                + "," + navCID + "||" + 1 + "," + 0;
                        sendDataOverSocket(showHide_str);


                        break;

                    case Constants.SET_SOUND:
                        functionCallCode = (separated[2].trim());


                        mTransmitter
                                .sendRequest(SLAPIMessageIds.SLAPI_SETSOUND,
                                        Integer.parseInt(separated[3]));

                        String setSound_str = Constants.SET_SOUND + "," + functionCallCode
                                + "," + navCID + "||" + 1 + "," + 0;
                        sendDataOverSocket(setSound_str);

                        break;

                    case Constants.GET_ROUTE_INFO:
                        functionCallCode = (separated[2].trim());

                        mTransmitter
                                .sendRequest(SLAPIMessageIds.SLAPI_GETROUTEINFO);
                        break;

                    case Constants.GET_LANGUAGE:
                        functionCallCode = (separated[2].trim());

                        mTransmitter
                                .sendRequest(SLAPIMessageIds.SLAPI_GETLANGUAGE);
                        break;

                    case Constants.ADD_WAY_POINT:
                        functionCallCode = (separated[2].trim());
                        Intent resultIntent2 = new Intent();

                        resultIntent2.putExtra("Latitude", Float.valueOf(separated[3].trim()));
                        resultIntent2.putExtra("Longitude", Float.valueOf(separated[4].trim()));
                        resultIntent2.putExtra("Order", Short.valueOf(separated[5].trim()));

                        Message msg2 = Message.obtain(null,
                                SLAPIMessageIds.SLAPI_SETWAYPOINTPOS);
                        msg2.setData(resultIntent2.getExtras());

                        // Show new route with set destination.

                        /*String paraStringAWP = separated[3] + "," + Float.valueOf(separated[4].trim()) + ","
                                + Float.valueOf(separated[5].trim());
                        try {
                            dbHelper.insertDataInSlappiRequestTable(
                                    new RequestItem(functionCallCode, Constants.ADD_WAY_POINT, paraStringAWP));
                        } catch (Exception e) {
                            e.printStackTrace();
                            ////Toast.makeText(context, "Failed to Insert SDP", //Toast.LENGTH_SHORT).show();
                        }*/

                        mTransmitter.sendRequest(msg2);
                        break;

                    case Constants.LOGIN_CODE:
                        if (separated[3].trim().equals("OK")) {
                            //dbHelper.deleteFromLogTableWithID(separated[2].trim());
                            ////Toast.makeText(context, "Login Successfully", //Toast.LENGTH_SHORT).show();

                            new CountDownTimer(4000, 4000) {
                                public void onTick(long millisUntilFinished) {

                                }

                                public void onFinish() {
                                    pingWorkerThread();


                                    Log.i("Calling After 2 seconds", "CALLING");
                                }
                            }.start();

                            new CountDownTimer(10000, 10000) {
                                public void onTick(long millisUntilFinished) {

                                }

                                public void onFinish() {
                                    //displayData();
                                    serverWorkerThread();
                                    Log.i("Calling After 2 seconds", "CALLING");
                                }
                            }.start();

                            //pingWorkerThread();
                            //serverWorkerThread();
                            PRIMARY_SERVER_RETRY_COUNTER = 0;
                            catchCounter = 0;
                        } else {
                            ////Toast.makeText(context, "Login Failed", //Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case Constants.PING_CODE:
                        if (separated[3].trim().equals("OK")) {
                            ////Toast.makeText(context, "PING SUCCESS", //Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case Constants.GPS_CODE:
                        ////Toast.makeText(SLAPIClientService.this, "IN GPS CODE CASE", //Toast.LENGTH_SHORT).show();
                        ////Toast.makeText(SLAPIClientService.this, "IN CASE OK:" + separated[3].trim(), //Toast.LENGTH_SHORT).show();
                        ////Toast.makeText(SLAPIClientService.this, "IN CASE ID:" + separated[2].trim(), //Toast.LENGTH_SHORT).show();
                        if (separated[3].trim().equals("OK")) {



                        } else {
                            ////Toast.makeText(context, "GPS ROW NOT Deleted", //Toast.LENGTH_SHORT).show();
                        }
                        break;


                    case Constants.SLAPPI_LICENSE_FILE:
                        functionCallCode = (separated[2].trim());


                        File dir1 = new File(Environment.getExternalStorageDirectory() + "/iGO/license/test.txt");

                        try {
                            if (!dir1.exists()) {
                                dir1.createNewFile();
                            }
                            FileWriter fw = new FileWriter(Environment.getExternalStorageDirectory() + "/iGO/license/test.txt", false);
                            fw.write((separated[3]));
                            fw.flush();
                            fw.close();

                            mHelloReceiver = new ServerHelloBroadcastReceiver();
                            mByeReceiver = new ServerByeBroadcastReceiver();

                            registerReceiver(mHelloReceiver, new IntentFilter(
                                    "com.navngo.igo.javaclient.SLAPI_SERVER_HELLO"));
                            registerReceiver(mByeReceiver, new IntentFilter(
                                    "com.navngo.igo.javaclient.SLAPI_SERVER_BYE"));

                            mTransmitter.doBindService();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;


                    default:
                        break;
                }

            }

            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
        }
    }
    public void setDestinationPosition(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;

        // GetAddressFromPOS
        mTransmitter
                .sendRequest(SLAPIMessageIds.SLAPI_GETADDRESSFROMPOS,
                        Float.floatToIntBits(latitude),
                        Float.floatToIntBits(longitude));

        // Intent resultIntent = new Intent();
        // resultIntent.putExtra("PlanMode", 0);
        // resultIntent.putExtra("Latitude", latitude);
        // resultIntent.putExtra("Longitude", longitude);
        //
        // Message msg = Message.obtain(null,
        // SLAPIMessageIds.SLAPI_SETDESTINATIONPOS);
        // msg.setData(resultIntent.getExtras());
        // showRouteDialog(msg);
    }
    public void showRouteDialog(String address) {
        if (dialog != null && dialog.isShowing())
            dialog.cancel();

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setCancelable(false);
        adb.setTitle("Route");
        adb.setMessage("Do you want to see route to destination:\n\n" + address);
        adb.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Delete any previous route.
                mTransmitter.sendRequest(SLAPIMessageIds.SLAPI_DELETEROUTE);

                Intent resultIntent = new Intent();
                resultIntent.putExtra("PlanMode", planModeFromServer);
                resultIntent.putExtra("Latitude", latitude);
                resultIntent.putExtra("Longitude", longitude);

                Message msg = Message.obtain(null,
                        SLAPIMessageIds.SLAPI_SETDESTINATIONPOS);
                msg.setData(resultIntent.getExtras());

                // Show new route with set destination.
                mTransmitter.sendRequest(msg);

                if (Constants.showIGO) {
                    Intent LaunchIntent = getPackageManager()
                            .getLaunchIntentForPackage(
                                    "com.navngo.igo.javaclient");
                    startActivity(LaunchIntent);
                }

            }
        });
        adb.setNegativeButton("Cancel", null);

        dialog = adb.create();
        dialog.getWindow()
                .setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

        dialog.show();
    }

    public void showRouteDialog(final Message msg) {
        // AlertDialog.Builder adb = new AlertDialog.Builder(this);
        // adb.setTitle("Route");
        // adb.setMessage("Do you want to see route.");
        // adb.setPositiveButton("Proceed", new OnClickListener() {
        // @Override
        // public void onClick(DialogInterface dialog, int which) {
        // // Delete any previous route.
        // mTransmitter.sendRequest(SLAPIMessageIds.SLAPI_DELETEROUTE);
        // // Show new route with set destination.
        // mTransmitter.sendRequest(msg);
        // }
        // });
        // adb.setNegativeButton("Cancel", null);
        //
        // AlertDialog dialog = adb.create();
        // dialog.getWindow()
        // .setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        //
        // dialog.show();
    }
    //
    public void getAddressFromPosition(float latitude, float longitude) {
        // mTransmitter
        // .sendRequest(SLAPIMessageIds.SLAPI_GETADDRESSFROMPOS,
        // Float.floatToIntBits(latitude),
        // Float.floatToIntBits(longitude));
    }

    /*protected void show//Toast(CharSequence message) {
        if (Constants.see//Toasts)
            ////Toast.makeText(this, message, //Toast.LENGTH_LONG).show();
    }*/

    protected void showMessege(CharSequence messageName, CharSequence content) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(messageName);
        adb.setMessage(content);
        adb.setPositiveButton("Ok", null);

        AlertDialog dialog = adb.create();
        dialog.getWindow()
                .setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

        if (Constants.seeDialogs)
            dialog.show();
    }

    public static String getGPSStatus() {
        mTransmitter
                .sendRequest(SLAPIMessageIds.SLAPI_GETGPSSTATUS);
        return "";
    }

    public static String deleteCurrentPlannedRoute() {
        mTransmitter
                .sendRequest(SLAPIMessageIds.SLAPI_DELETEROUTE);
        return "Success";
    }

    public static String setPlanMode(int mode) {
        mTransmitter
                .sendRequest(SLAPIMessageIds.SLAPI_SETPLANMODE,
                        mode);
        return "";
    }

    public static String setVehicleType(int type) {
        mTransmitter
                .sendRequest(SLAPIMessageIds.SLAPI_SETVEHICLE,
                        type);
        return "";
    }

    public static String getRouteInfo() {
        mTransmitter
                .sendRequest(SLAPIMessageIds.SLAPI_GETROUTEINFO);
        return "";
    }

    public static String addFavourite(String name, float lat, float lng) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("Name", name);
        resultIntent.putExtra("Latitude", lat);
        resultIntent.putExtra("Longitude", lng);

        Message msg = Message.obtain(null,
                SLAPIMessageIds.SLAPI_ADDFAVORITEPOS);
        msg.setData(resultIntent.getExtras());

        // Show new route with set destination.
        mTransmitter.sendRequest(msg);

		/*CPD cpd = new CPD();
        *//*mTransmitter
                .sendRequest(SLAPIMessageIds.SLAPI_ADDFAVORITEPOS,"TEST Favourite",Float.floatToIntBits(lat),
						Float.floatToIntBits(lng));*//*
        cpd.addString(name); // Name of the function
		cpd.addDouble(lat);
		cpd.addDouble(lng);
		mTransmitter
				.sendRequest(Message.obtain(null, SLAPIMessageIds.SLAPI_ADDFAVORITEPOS) ,cpd.getBytes());*/


        return "";
    }

    public static String setLanguage(int language) {
        mTransmitter
                .sendRequest(SLAPIMessageIds.SLAPI_SETLANGUAGE,
                        language);
        return "";
    }

    public static String getLanguage() {
        mTransmitter
                .sendRequest(SLAPIMessageIds.SLAPI_GETLANGUAGE);
        return "";
    }

    static boolean hideShow = true;

    public static String showHideiGO() {
        if (hideShow) {
            hideShow = false;
            mTransmitter
                    .sendRequest(SLAPIMessageIds.SLAPI_SHOWHMI, 0);
        } else {
            hideShow = true;
            mTransmitter
                    .sendRequest(SLAPIMessageIds.SLAPI_SHOWHMI, 1);
        }

        return "";
    }

    Context context = SLAPIClientService.this;
    static boolean onOff = true;

    public static String muteUnmuteiGO() {
        if (onOff) {
            onOff = false;
            mTransmitter
                    .sendRequest(SLAPIMessageIds.SLAPI_SETSOUND, 0);
            ////Toast.makeText(CommonObjects.getContext(), "OFF", //Toast.LENGTH_SHORT).show();

        } else {
            onOff = true;
            mTransmitter
                    .sendRequest(SLAPIMessageIds.SLAPI_SETSOUND, 1);
            ////Toast.makeText(CommonObjects.getContext(), "ON", //Toast.LENGTH_SHORT).show();
        }
        return "";
    }
    public static void addWayPoints(float lat, float lng, short order) {
        Intent resultIntent = new Intent();

        resultIntent.putExtra("Latitude", lat);
        resultIntent.putExtra("Longitude", lng);
        resultIntent.putExtra("Order", order);

        Message msg = Message.obtain(null,
                SLAPIMessageIds.SLAPI_SETWAYPOINTPOS);
        msg.setData(resultIntent.getExtras());

        // Show new route with set destination.
        mTransmitter.sendRequest(msg);

    }
    private void gpsWorkerThread() {
        if (gpsThread != null)
            gpsThread.interrupt();

        gpsHandler = new Handler(Looper.getMainLooper());
        gpsThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    while (true) {
                        Log.i("IN LOOP gps", "loop");
                        gpsHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                getGpsDataFromSlappi();
                            }
                        });
                        /*Thread.sleep(CommonMethods.getIntPreference(MainActivity.this,
                                Constants.KeyValues.THREAD_DETAILS, Constants.KeyValues.AUTO_REFRESH_TIME, 50000));*/
                        Thread.sleep(Constants.GPS_THREAD_TIME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        gpsThread.start();
    }
    private void getGpsDataFromSlappi() {

        try {
            //isiGO_Started = true;
            isFromGpsWorkerThread = true;
            mTransmitter
                    .sendRequest(SLAPIMessageIds.SLAPI_GETGPSSTATUS);
        } catch (Exception e) {
            e.printStackTrace();
            isGPSWorkerThreadRunning = false;

        }


    }
    private void serverWorkerThread() {
        if (serverThread != null)
            serverThread.interrupt();

        serverHandler = new Handler(Looper.getMainLooper());
        serverThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    while (true) {
                        Log.i("IN LOOP gps", "loop");
                        serverHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                // if (!isSendGPSLogInProcess) {
                                getDataFromDB_SendToServer();
                                // }

                            }
                        });
                        /*Thread.sleep(CommonMethods.getIntPreference(MainActivity.this,
                                Constants.KeyValues.THREAD_DETAILS, Constants.KeyValues.AUTO_REFRESH_TIME, 50000));*/
                        Thread.sleep(Constants.SERVER_THREAD_TIME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        serverThread.start();
    }
    private void pingWorkerThread() {
        if (pingThread != null)
            pingThread.interrupt();

        pingHandler = new Handler(Looper.getMainLooper());
        pingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    while (true) {
                        Log.i("IN LOOP gps", "loop");
                        pingHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                pingServer();
                            }
                        });
                        /*Thread.sleep(CommonMethods.getIntPreference(MainActivity.this,
                                Constants.KeyValues.THREAD_DETAILS, Constants.KeyValues.AUTO_REFRESH_TIME, 50000));*/
                        Thread.sleep(Constants.PING_THREAD_TIME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        pingThread.start();
    }
    boolean isGPSWorkerThreadRunning = false;
    private void pingServer() {
        try {
            // if (mSocket.isConnected()) {
            // try {

            String pingString = /*Constants.DATA_FROM_DEVICE + "," + */Constants.PING_CODE + "," + "00" + "," + navCID;

            sendDataOverSocket(pingString);
            //sendPING_DataOverSocket(pingString);

            if (isIGORunning("com.navngo.igo.javaclient")) {
                /*final Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.navngo.igo.javaclient");
                if (launchIntent != null) {*/

                if (!isGPSWorkerThreadRunning) {
                    new CountDownTimer(4000, 4000) {
                        public void onTick(long millisUntilFinished) {
                            isGPSWorkerThreadRunning = true;
                        }

                        public void onFinish() {
                            //pingWorkerThread();
                            //startActivity(launchIntent);
                            connectSlappi();
                            Log.i("Calling After 2 seconds", "CALLING");
                        }
                    }.start();
                }


                // }
            }else{
                isGPSWorkerThreadRunning = false;
            }


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
    }
    public void getDataFromDB_SendToServer() {

        try {
            if (mSocket.isConnected()) {
                try {


                        String toServer =  "";
                        sendDataOverSocket(toServer);


                    //}


                } catch (Exception e) {
                    e.printStackTrace();
                    ////Toast.makeText(context, "ERROR getDataFromDB_SendToServer", //Toast.LENGTH_SHORT).show();
                }

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void sendDataOverSocket(String data) {

        if (mSocket.isConnected()) {
            try {

                DataOutputStream output_to_server = new DataOutputStream(mSocket.getOutputStream());
                output_to_server.writeBytes(Constants.DATA_FROM_DEVICE + "," + data + "\r\n");
                output_to_server.flush();
                catchCounter = 0;

            } catch (IOException e) {
                e.printStackTrace();
                //Toast.makeText(context, "in CATCH sendDataOverSocket", Toast.LENGTH_SHORT).show();

                // mClientTask.cancel(true);

                /*if (serverThread != null)
                    serverThread.interrupt();*/

                catchCounter = catchCounter + 1;

                if (catchCounter < 3) {
                    //Toast.makeText(context, "in if Counter:" + catchCounter, Toast.LENGTH_SHORT).show();

                    mClientTask = (MyClientTask) new MyClientTask(
                            //Constants.PRIMARY_SERVER_IP_ADDRESS, Constants.SERVER_PORT)
                            Constants.SECONDARY_SERVER_IP_ADDRESS, Constants.SERVER_PORT)
                            .execute();
                } else {
                    //Toast.makeText(context, "in else Counter:" + catchCounter, Toast.LENGTH_SHORT).show();

                    mClientTask = (MyClientTask) new MyClientTask(
                            Constants.SECONDARY_SERVER_IP_ADDRESS, Constants.SERVER_PORT)
                            .execute();
                }


                //PRIMARY_SERVER_RETRY_COUNTER++;*/


                /*if (PRIMARY_SERVER_RETRY_COUNTER < 2) {
                    PRIMARY_SERVER_RETRY_COUNTER++;
                    //Toast.makeText(context, "PRIMARY_SERVER_RETRY_COUNTER " + PRIMARY_SERVER_RETRY_COUNTER, Toast.LENGTH_SHORT).show();
                    mClientTask.cancel(true);
                    mClientTask = (MyClientTask) new MyClientTask(
                            Constants.PRIMARY_SERVER_IP_ADDRESS, Constants.TEST_SERVER_PORT)
                            .execute();
                } else {
                    SECONDARY_SERVER_RETRY_COUNTER++;
                    //Toast.makeText(context, "SECONDARY_SERVER_RETRY_COUNTER " + SECONDARY_SERVER_RETRY_COUNTER, Toast.LENGTH_SHORT).show();

                    mClientTask.cancel(true);
                    mClientTask = (MyClientTask) new MyClientTask(
                            Constants.PRIMARY_SERVER_IP_ADDRESS, Constants.SERVER_PORT)
                            .execute();
                }
*/
                /*if (gpsThread != null)
                    gpsThread.interrupt();


                //Toast.makeText(context, "Fail to ping, Socket not connected", Toast.LENGTH_SHORT).show();

                if (PRIMARY_SERVER_RETRY_COUNTER < 4) {
                    PRIMARY_SERVER_RETRY_COUNTER++;
                    //if (mClientTask != null) {
                    // mClientTask.cancel(true);
                    // }
                    //Toast.makeText(context, "PRIMARY_SERVER_RETRY_COUNTER " + PRIMARY_SERVER_RETRY_COUNTER, Toast.LENGTH_SHORT).show();

                    mClientTask = (MyClientTask) new MyClientTask(
                            Constants.PRIMARY_SERVER_IP_ADDRESS, Constants.TEST_SERVER_PORT)
                            .execute();

                } else if (SECONDARY_SERVER_RETRY_COUNTER < 4) {
                    SECONDARY_SERVER_RETRY_COUNTER++;
                    //Toast.makeText(context, "SECONDARY_SERVER_RETRY_COUNTER " + SECONDARY_SERVER_RETRY_COUNTER, Toast.LENGTH_SHORT).show();

                    // if (mClientTask != null) {
                    //mClientTask.cancel(true);
                    // }
                    *//*mClientTask = (MyClientTask) new MyClientTask(
                            Constants.SECONDARY_SERVER_IP_ADDRESS, Constants.SERVER_PORT)
                            .execute();*//*
                    mClientTask = (MyClientTask) new MyClientTask(
                            Constants.PRIMARY_SERVER_IP_ADDRESS, Constants.SERVER_PORT)
                            .execute();

                } else {
                    PRIMARY_SERVER_RETRY_COUNTER = 0;
                    SECONDARY_SERVER_RETRY_COUNTER = 0;

                    //Toast.makeText(context, "ALL 0 ", Toast.LENGTH_SHORT).show();


                    // if (mClientTask != null) {
                    // mClientTask.cancel(true);
                    // }

                    if (pingThread != null)
                        pingThread.interrupt();
                        *//*

                    if (gpsThread != null)
                        gpsThread.interrupt();*//*

                    countDownTimer.start();
                }*/

                //addNotification(dstAddress, "RETRY_COUNT / 2 UnknownHostException");
            }

        } else {
            //Toast.makeText(context, "Socket Not Connected", Toast.LENGTH_SHORT).show();
        }
    }
    private void getNavCID() {
        BufferedReader br;
        String line;

        String cid1 = "";
        String cid2 = "";
        String cid3 = "";
        String sid1 = "";
        String sid1part1 = "";
        String sid1part2 = "";
        String sid2 = "";
        String sid2part1 = "";
        String sid2part2 = "";
        String sid3 = "";
        String sid3part1 = "";
        String sid3part2 = "";

        String SD1CID = "";
        String SD1SDID = "";
        String SD2CID = "";
        String SD2SDID = "";
        String internalCid = "";
        String internalSDID = "";
        StringBuilder text;


        File fileread = new File("/sys/block/mmcblk0/../../cid");
        if (fileread.exists()) {
            text = new StringBuilder();
            try {
                br = new BufferedReader(new FileReader(fileread));
                while (true) {
                    line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    cid1 = line;
                    sid1part1 = cid1.substring(0, 2);
                    sid1part2 = cid1.substring(18, 26);
                    sid1 = sid1part1 + sid1part2;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        internalCid = cid1;
        internalSDID = sid1;

        if (!isSDID_SET) {
            if (!internalSDID.equals("") || internalSDID != "") {
                isSDID_SET = true;
                CommonMethods.setStringPreference(getApplicationContext(), Constants.USER_DETAILS, Constants.NAV_CID, internalSDID);
            }
        }


        File fileread1 = new File("/sys/block/mmcblk1/../../cid");
        if (fileread1.exists()) {
            text = new StringBuilder();
            try {
                br = new BufferedReader(new FileReader(fileread1));
                while (true) {
                    line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    cid2 = line;
                    sid2part1 = cid2.substring(0, 2);
                    sid2part2 = cid2.substring(18, 26);
                    sid2 = sid2part1 + sid2part2;
                }
            } catch (IOException e2) {
            }
        }
        SD1CID = cid2;
        SD1SDID = sid2;
        if (!isSDID_SET) {
            if (!internalSDID.equals("") || internalSDID != "") {
                isSDID_SET = true;
                CommonMethods.setStringPreference(getApplicationContext(), Constants.USER_DETAILS, Constants.NAV_CID, SD1SDID);
            }
        }

        File fileread2 = new File("/sys/block/mmcblk2/../../cid");
        if (fileread2.exists()) {
            text = new StringBuilder();
            try {
                br = new BufferedReader(new FileReader(fileread2));
                while (true) {
                    line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    cid3 = line;
                    sid3part1 = cid3.substring(0, 2);
                    sid3part2 = cid3.substring(18, 26);
                    sid3 = sid3part1 + sid3part2;
                }
            } catch (IOException e3) {
            }
        }
        SD2CID = cid3;
        SD2SDID = sid3;

        if (!isSDID_SET) {
            if (!internalSDID.equals("") || internalSDID != "") {
                isSDID_SET = true;
                CommonMethods.setStringPreference(getApplicationContext(), Constants.USER_DETAILS, Constants.NAV_CID, SD2SDID);
            }
        }

    }
    boolean isSDID_SET = false;
    boolean isiGO_Started = false;
    public boolean isIGORunning(String process) {
        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).processName.equals(process)) {
                return true;
            }
        }
        return false;
    }
}
