package mdm.tw.com.mdm.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;

import mdm.tw.com.mdm.helpers.Constants;
import mdm.tw.com.mdm.services.MyService;
import mdm.tw.com.mdm.utils.CommonMethods;

/**
 * Created by developer on 5/31/18.
 */

public class BatteryChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent batteryStatus) {
        if ("android.intent.action.BATTERY_CHANGED".equals(batteryStatus.getAction())) {
            System.out.println("android.intent.action.BATTERY_CHANGED");

            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean bCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;
            if(bCharging){
//            charging=true;
//            flag=true;
                Log.d("charging:","true");
            }
            else{

                Log.d("charging:","flase");
//            charging=false;
            }
            int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
            int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

            float batteryPct = level / (float) scale;
       if(CommonMethods.getIntPreference(context, Constants.PREF_MDM, Constants.POLL_BATTERY_LOW_LEVEL,0) >= (batteryPct*100)){
           MyService service = new MyService();
           // waqas changes
//           service.save_values("7");
        }
            int curStatus = (int) (batteryPct * 100);
            System.out.println("Current status : " + curStatus);
        }
    }
}
