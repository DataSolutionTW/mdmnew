package mdm.tw.com.mdm.Slapi.RoutePush;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

public class SLAPITransmitter {
	private final static String logname = "SLAPIClient";
	private boolean mVerbose = true;
	private Activity mActivity;
	private Service mService;
	private Messenger mMessenger;

	SLAPITransmitter(Activity theActivity, Messenger messenger) {
		init(theActivity, messenger);
	}

	SLAPITransmitter(Service theService, Messenger messenger) {
		init(theService, messenger);
	}

	public void init(Activity theActivity, Messenger messenger) {
		mActivity = theActivity;
		mMessenger = messenger;
	}

	public void init(Service theService, Messenger messenger) {
		mService = theService;
		mMessenger = messenger;
	}

	public void setVerbose(boolean really) {
		mVerbose = really;
	}

	/** Informs the user on some event */
	public void showToastShort(CharSequence text) {
		setVerbose(Constants.seeToasts);

		if (mVerbose) {
			if (mActivity != null)
				Toast.makeText(mActivity, text, Toast.LENGTH_SHORT).show();
			else if (mService != null)
				Toast.makeText(mService, text, Toast.LENGTH_SHORT).show();
		}
		Log.d(logname, (String) text);
	}

	/** Messenger for communicating with service. */
	Messenger messengerService = null;
	/** Flag indicating whether we have called bind on the service. */
	boolean mIsBound;

	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service. We are communicating with our
			// service through an IDL interface, so get a client-side
			// representation of that from the raw service object.
			messengerService = new Messenger(service);

			// As part of the sample, tell the user what happened.
			showToastShort("SLAPI service connected");

			if (mService != null)
				((SLAPIClientService) mService).clientFirstHandshake(false);
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			messengerService = null;
			showToastShort("SLAPI server disconnected.");
		}
	};

	void doBindService() {
		// Establish a connection with the service. We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.
        Intent i = new Intent(SLAPIMessageIds.SLAPIServiceName);
        i.setPackage("com.navngo.igo.javaclient");
		if (mActivity != null)
			mActivity.bindService(i,
					mConnection, Context.BIND_AUTO_CREATE);
		else if (mService != null)
			mService.bindService(i,
					mConnection, Context.BIND_AUTO_CREATE);
		mIsBound = true;
		showToastShort("SLAPI binding.");
	}

	void doUnbindService() {
		if (mIsBound) {
			// If we have received the service, and hence registered with
			// it, then now is the time to unregister.
			if (messengerService != null) {
				sendRequest(SLAPIMessageIds.SLAPI_CLIENT_BYE,
						mMessenger.hashCode());
			}

			// Detach our existing connection.
			if (mActivity != null)
				mActivity.unbindService(mConnection);
			else if (mService != null)
				mService.unbindService(mConnection);
			mIsBound = false;
			showToastShort("SLAPI unbinding.");
		}
	}

	/** Sends a simple (2 ints) request to the server */
	public void sendRequest(int msgId, int wParam, int lParam) {
		Message msg = Message.obtain(null, msgId, wParam, lParam);
		sendRequest(msg);
	}

	/** Sends a simple (1 int) request to the server */
	public void sendRequest(int msgId, int wParam) {
		Message msg = Message.obtain(null, msgId, wParam, 0);
		sendRequest(msg);
	}

	/** Sends a simple (no args) request to the server */
	public void sendRequest(int msgId) {
		Message msg = Message.obtain(null, msgId);
		sendRequest(msg);
	}

	public void sendRequest(int msgId, String rID) {
		Message msg = Message.obtain(null, msgId);
		sendRequest(msg);
	}

	/** Send a byte array based request to the server */
	public void sendRequest(Message msg, byte[] cpd) {
		msg.getData().putByteArray("Bytes", cpd);

		sendRequest(msg);
	}

	/** Send a byte array based request to the server *//*
	public void sendRequest(Message msg,int rID) {
		msg.getData().putInt("ID", rID);

		sendRequest(msg);
	}*/

	/** Sends a request to the server */
	public void sendRequest(Message msg) {
		showToastShort("Sending " + SLAPIMessageIds.MessageName(msg.what)
				+ " to the SLAPI server.");

		System.out.println("Sending " + SLAPIMessageIds.MessageName(msg.what)
				+ " to the SLAPI server.");

		msg.replyTo = mMessenger;

		try {
			messengerService.send(msg);
			// if (mActivity != null)
			// ((SLAPIClient) mActivity).logMessage(msg);

		} catch (RemoteException e) {
			// There is nothing special we need to do if the service
			// has crashed.
		}
	}

	/** Sends a request to the server *//*
	public void sendRequest(Message msg, int rID) {
		showToastShort("Sending " + SLAPIMessageIds.MessageName(msg.what)
				+ " to the SLAPI server.");

		System.out.println("Sending " + SLAPIMessageIds.MessageName(msg.what)
				+ " to the SLAPI server.");

		msg.replyTo = mMessenger;
		msg.arg1 = rID;

		try {
			messengerService.send(msg);
			// if (mActivity != null)
			// ((SLAPIClient) mActivity).logMessage(msg);

		} catch (RemoteException e) {
			// There is nothing special we need to do if the service
			// has crashed.
		}
	}*/
}
