package mdm.tw.com.mdm.models;

/**
 * Created by TW on 8/16/2017.
 */

public class Internet {
    public long getData() {
        return data;
    }

    public void setData(long data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    long data;
    String date;
}
