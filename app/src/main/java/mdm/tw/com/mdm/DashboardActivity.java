package mdm.tw.com.mdm;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import mdm.tw.com.mdm.Slapi.RoutePush.SLAPIClientService;
import mdm.tw.com.mdm.db.DbHelper;
import mdm.tw.com.mdm.helpers.PrefrenceUtils;
import mdm.tw.com.mdm.helpers.UtilsFunctions;
import mdm.tw.com.mdm.interfaces.GetRecordValue;
import mdm.tw.com.mdm.services.MyService;
import mdm.tw.com.mdm.utils.DeviceAdmin;

/**
 * Created by developer on 9/18/18.
 */

public class DashboardActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GetRecordValue {

    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    Button route;
    TelephonyManager telephonyManager = null;
    private static final int REQUEST_CODE = 0;
    private DevicePolicyManager mDPM;
    private ComponentName mAdminName;
    TextView txtRecords, txtImei, txtSpeed, txtDistance;
    //    private ArrayList<LocationItem> lists;
    DbHelper db;
    public static GetRecordValue objGetRecordValue;

    String lon = "74.3151";
    String lat = "31.5879";
    String lon_2 = "74.3236";
    String lat_2 = "31.5832";
    String lon_3 = "74.3824";
    String lat_3 = "31.5855";
    String type = "drive";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.map_fragment);


        txtImei = (TextView) findViewById(R.id.valImei);
        txtImei.setText(PrefrenceUtils.getImei(getApplicationContext()));
        txtRecords = (TextView) findViewById(R.id.valRecords);
        txtDistance = (TextView) findViewById(R.id.valDistance);
        txtSpeed = (TextView) findViewById(R.id.valSpeed);
        route = (Button)findViewById(R.id.route);
        addForceStopSolution();

        // Create Progress Bar.
        SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(
                R.id.mapView));
        mapFragment.getMapAsync(this);

        startService(new Intent(this, MyService.class));
        Intent i = new Intent(DashboardActivity.this,SLAPIClientService.class);
        i.putExtra("value","RequestForCall");
        i.putExtra("data","data");
        startService(i);
        getImei();
        db = DbHelper.getInstance(getApplicationContext());
        objGetRecordValue = this;

        route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String str = "com.sygic.aura://coordinate|" + lon + "|" + lat + "|" + lon_2 +"|"+ lat_2 + "|"+ lon_3 + "|" + lat_3 + "|" +type;
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(str)));
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
//        if (mGoogleApiClient != null) {
//            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
//        }


    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        System.out.println("calling : onMapReady");
        mGoogleMap=googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
//                checkLocationPermission();
                handleMapPermission();
            }
        }
        else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        System.out.println("calling : onConnected");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            System.out.println("calling : onConnected Granted");
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location)
    {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }


        //Place current location marker
//        LatLng latLng = new LatLng(32.778205, -96.798971);
        PrefrenceUtils.saveGoogleLocation(getApplicationContext(), location);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
//        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,11));

    }

    public void handleMapPermission(){

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {

                        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                android.Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {
                            //Location Permission already granted
                            buildGoogleApiClient();
                            mGoogleMap.setMyLocationEnabled(true);

                        }

                        // permission is granted, open the camera
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }



    public void getImei() {


        if ((int) Build.VERSION.SDK_INT < 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                getDeviceId();
            }

        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                handlePhoneStatePermission();
            } else {

                System.out.println("Permission is granted");
                getDeviceId();
            }

        }


//        imei.setText("IMEI: " + sid1);
//        imsi.setText("IMSI id: " + imei2 + "\nNumber of records:" + lists.size()+"\n"+a);
    }

    public void handlePhoneStatePermission() {

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_PHONE_STATE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        handleLocationPermission();
                        getDeviceId();
                        // permission is granted, open the camera
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                            handleLocationPermission();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void handleLocationPermission() {

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        handleLocationCoursePermission();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                            handleLocationCoursePermission();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void handleLocationCoursePermission() {

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void getDeviceId() {
        String deviceID = "";

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (telephonyManager != null) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    android.Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {

                deviceID = getDeviceID(telephonyManager);
                if(deviceID.contains("NONE")){

                    Toast.makeText(getApplicationContext(), "deviceID is NONE", Toast.LENGTH_SHORT).show();

                    System.out.println("Telephony manager not null");
                    PrefrenceUtils.saveImei(this, UtilsFunctions.getSid());
                    System.out.println("Device id in sid is : " + UtilsFunctions.getSid());
                    this.txtImei.setText(UtilsFunctions.getSid());

                }
                else if(deviceID.contains("123456") || deviceID.equals("")){
                    Toast.makeText(getApplicationContext(), "deviceID.contains(\"123456\")", Toast.LENGTH_SHORT).show();
                    PrefrenceUtils.saveImei(this, UtilsFunctions.getAndroidID(getApplicationContext()));
                    this.txtImei.setText(UtilsFunctions.getAndroidID(getApplicationContext()));
                }
                else{

                   // Toast.makeText(getApplicationContext(), "deviceID", Toast.LENGTH_SHORT).show();

                    System.out.println("Device of sid id is : " + UtilsFunctions.getSid());
                    System.out.println("Device id is : " + deviceID);
                    PrefrenceUtils.saveImei(this, deviceID);
                    this.txtImei.setText(deviceID);
                }

//                System.out.println("Device id using Settings"+  Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));

            }
        } else {

            Toast.makeText(getApplicationContext(), "Telephony manager is null", Toast.LENGTH_SHORT).show();

            System.out.println("Device id of android id : "+UtilsFunctions.getAndroidID(getApplicationContext()));

            PrefrenceUtils.saveImei(this, UtilsFunctions.getAndroidID(getApplicationContext()));
            this.txtImei.setText(UtilsFunctions.getAndroidID(getApplicationContext()));
            System.out.println("Telephony manager is null");
            System.out.println("Device id in sid is : " + UtilsFunctions.getSid());
        }
    }

    String getDeviceID(TelephonyManager phonyManager) {

        String id = "";
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED) {

            id = phonyManager.getDeviceId();
        }
        if (id == null) {
            id = "not available";
        }

        int phoneType = phonyManager.getPhoneType();
        switch (phoneType) {
            case TelephonyManager.PHONE_TYPE_NONE:
                return "" + id;

            case TelephonyManager.PHONE_TYPE_GSM:
                return "" + id;

            case TelephonyManager.PHONE_TYPE_CDMA:
                return "" + id;

            default:
                return "" + id;
        }
    }

    public void addForceStopSolution(){
        try
        {
            // Initiate DevicePolicyManager.
            mDPM = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
            // Set DeviceAdminDemo Receiver for active the component with different option
            mAdminName = new ComponentName(this, DeviceAdmin.class);

            if (!mDPM.isAdminActive(mAdminName)) {
                // try to become active
                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Click on Activate button to secure your application.");
                startActivityForResult(intent, REQUEST_CODE);
            }
            else
            {
                // Already is a device administrator, can do security operations now.
//                    mDPM.lockNow();
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(REQUEST_CODE == requestCode)
        {
            if(requestCode == Activity.RESULT_OK)
            {
                System.out.println("Activity.RESULT_OK");
                // done with activate to Device Admin
            }
            else
            {
                System.out.println("Activity.RESULT_NOT_OK");
                // cancle it.
            }
        }
    }

    @Override
    public void showRecordValue(int value) {

        System.out.println("No of records : "+value);
        txtRecords.setText(""+value);

    }

    @Override
    public void showSpeedValues(String speed) {
        txtSpeed.setText(speed+" KM/h");

    }

    @Override
    public void showDistanceValues(String distance) {
        txtDistance.setText(distance+" M");

    }

    @Override
    public void showDirectionValues(String direction) {
//        txtDirection.setText(direction);

    }

    @Override
    public void showAltitudeValues(String altitude) {
//        txtAltitude.setText(altitude);

    }

    @Override
    public void showLatLngValues(String latLng) {
//        txtLatLng.setText(latLng);

    }

}
