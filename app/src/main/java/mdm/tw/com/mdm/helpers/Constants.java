package mdm.tw.com.mdm.helpers;

/**
 * Created by developer on 5/22/18.
 */

public class Constants {

    public static String PREF_MDM = "PREF_MDM";
    public static String IMEI = "IMEI";
    public static String PRIMARY_IP = "PrimaryIP";
    public static String SECONDARY_IP = "SecondaryIP";
    public static final String CONNECT_TO_WIFI = "WIFI";
    public static final String CONNECT_TO_MOBILE = "MOBILE";
    public static final String NOT_CONNECT = "NOT_CONNECT";
    public final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    public static String POLL_INTERVAL = "PollInterval";
    public static String POLL_ANGLE_CHANGE = "PollAngleChange";
    public static String POLL_DISTANCE_COVERED = "PollDistnaceCovered";
    public static String POLL_BATTERY_LOW_LEVEL = "PollBatteryLowLevel";

    public static String GOOGLE_LOCATION_LAT = "GOOGLE_LOCATION_LAT";
    public static String GOOGLE_LOCATION_LONG = "GOOGLE_LOCATION_LONG";
    public static String MOBILE_BYTES = "MOBILE_BYTES";


}
