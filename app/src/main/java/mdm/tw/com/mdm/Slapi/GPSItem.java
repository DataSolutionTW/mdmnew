package mdm.tw.com.mdm.Slapi;

/**
 * Created by DaNii on 04/11/2016.
 */

public class GPSItem {


    private String localID;
    private String GPSFix;
    private String NofSatellitesTracked;
    private String NofSatellitesinView;
    private String HDOP;
    private String UtcTime;
    private String Latitude;
    private String Longitude;
    private String Altitude;
    private String Course;
    private String Speed;

    public GPSItem() {
    }

    public String getLocalID() {
        return localID;
    }

    public void setLocalID(String localID) {
        this.localID = localID;
    }

    public String getGPSFix() {
        return GPSFix;
    }

    public void setGPSFix(String GPSFix) {
        this.GPSFix = GPSFix;
    }

    public String getNofSatellitesTracked() {
        return NofSatellitesTracked;
    }

    public void setNofSatellitesTracked(String nofSatellitesTracked) {
        NofSatellitesTracked = nofSatellitesTracked;
    }

    public String getNofSatellitesinView() {
        return NofSatellitesinView;
    }

    public void setNofSatellitesinView(String nofSatellitesinView) {
        NofSatellitesinView = nofSatellitesinView;
    }

    public String getHDOP() {
        return HDOP;
    }

    public void setHDOP(String HDOP) {
        this.HDOP = HDOP;
    }

    public String getUtcTime() {
        return UtcTime;
    }

    public void setUtcTime(String utcTime) {
        UtcTime = utcTime;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getAltitude() {
        return Altitude;
    }

    public void setAltitude(String altitude) {
        Altitude = altitude;
    }

    public String getCourse() {
        return Course;
    }

    public void setCourse(String course) {
        Course = course;
    }

    public String getSpeed() {
        return Speed;
    }

    public void setSpeed(String speed) {
        Speed = speed;
    }
}

